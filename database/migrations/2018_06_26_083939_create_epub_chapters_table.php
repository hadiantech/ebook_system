<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpubChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epub_chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('epub_draft_id');
            $table->string('title');
            $table->text('body');
            $table->integer('order');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('epub_draft_id')
                ->references('id')->on('epub_drafts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epub_chapters');
    }
}
