<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            $table->date('started_at')->nullable()->change();
            $table->date('ended_at')->nullable()->change();
            $table->integer('promotion')->nullable()->after('ended_at');
            $table->integer('store_inv')->nullable()->after('ended_at');
            $table->integer('store')->nullable()->after('ended_at');
            $table->integer('books_created')->nullable()->after('ended_at');
            $table->integer('books_upload')->nullable()->after('ended_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            //
        });
    }
}
