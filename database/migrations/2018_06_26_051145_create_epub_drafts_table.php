<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpubDraftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('epub_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('title');
            $table->string('creator');
            $table->string('subject');
            $table->text('description');
            $table->string('publisher');
            $table->string('contributor');
            $table->date('published_at');
            $table->string('type');
            $table->string('format');
            $table->string('source');
            $table->string('language');
            $table->string('relation');
            $table->string('coverage');
            $table->string('rights');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epub_drafts');
    }
}
