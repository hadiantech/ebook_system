<?php

use Illuminate\Database\Seeder;

class GeneralStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('store')->insert([
       	[
            'store_name' => 'Books',
            'type' => 'General',
            'status' => 'Active',
        ],
        [
            'store_name' => 'Comic',
            'type' => 'General',
            'status' => 'Active',
        ],
        [
            'store_name' => 'Free',
            'type' => 'General',
            'status' => 'Active',
        ]

    	]);
    }
}
