<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Auth::routes();

Route::group( ['middleware' => ['auth']], function() {
    Route::get('/dashboard',  'HomeController@dashboard')->name('dashboard');
    
    Route::get('/profile',  'HomeController@profile')->name('profile');
    Route::get('/profile/edit',  'HomeController@profile_edit')->name('profile.edit');
    Route::get('/profile/password',  'HomeController@profile_edit_pw')->name('profile.edit_pw');
    Route::put('/profile/update',  'HomeController@profile_update')->name('profile.update');
    Route::put('/profile/password/update',  'HomeController@profile_pw_update')->name('profile.pw_update');

    Route::get('/analytics',  'AnalyticsController@index')->name('analytics');
    Route::get('/payments',  'PaymentsController@index')->name('payments');
    Route::get('/payments/show',  'PaymentsController@show')->name('payments.show');
    Route::get('/payments/edit',  'PaymentsController@edit')->name('payments.edit');
    Route::get('/payments/selectpayment',  'PaymentsController@selectpayment')->name('payments.selectpayment');
    Route::get('/payments/history',  'PaymentsController@history')->name('payments.history');
    
    
    Route::resource('users', 'UserController');
    Route::get('/users/password/{id}/edit', 'UserController@edit_pw')->name('users.edit_pw');    
    Route::put('/users/password/{id}', 'UserController@update_pw')->name('users.update_pw');    

    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
    Route::resource('tags', 'TagController');
    Route::resource('publishers', 'PublisherController');
    Route::resource('books', 'BookController');
    Route::resource('stores', 'StoreController');
    Route::resource('packages', 'PackageController');
    Route::resource('storeInvs', 'Store_InvController');

    Route::get('/stores/{store}/payout',  'StoreController@payout')->name('store.payout');


    //datatables
    Route::any('datatables/getUser', 'DatatablesController@getUser');
    Route::any('datatables/getTag', 'DatatablesController@getTag');
    Route::any('datatables/getPublisher', 'DatatablesController@getPublisher');
    Route::any('datatables/getBook', 'DatatablesController@getBook');
    Route::any('datatables/getStore', 'DatatablesController@getStore');
    Route::any('datatables/getStoreInv/{id}', 'DatatablesController@getStoreInv');
    
    //epubcreator
    Route::resource('creator', 'EpubCreatorController');
    Route::resource('creator.chapter', 'EpubChapterController');
    Route::get('/creator/{draft}/publish', 'EpubCreatorController@publish')->name('creator.publish'); 

    
    //select2
    Route::get('select2/getUser', function(){
        return App\User::whereDoesntHave('publisher', function ($query) {
            $query->where('name','LIKE','%'.request('q').'%')
            ->orWhere('username','LIKE','%'.request('q').'%')                
            ->orWhere('email','LIKE','%'.request('q').'%');
        })->paginate(10);
    });

    Route::get('select2/getBook', function(){
        return App\Publisher::where('name','LIKE','%'.request('q').'%')
            ->paginate(10);
    });

    Route::get('ajax/getBookByID', function(){
        $book= App\Book::find(request('book_id'));

        if(!$book->getMedia('cover')->first())
        $image="http://via.placeholder.com/300x300";
         else
        $image=$book->getMedia('cover')->first()->getUrl();

       // $image =$book->getMedia('cover')->first();
        return Response::json(array("book"=>$book, "image"=>$image));
    });



    Route::get('select2/getNotInStoreBook', function(){
    $store_id = request('store_id');
    $store=App\Store::find($store_id);
    $instoreinv = App\StoreInv::where('store_id', $store_id)->pluck('book_id');
    // $books = App\Book::whereNotIn('id',$instoreinv)->get();
    //$books; exit;
    return App\Book::where('name','LIKE','%'.request('q').'%')->whereNotIn('id',$instoreinv)
            ->paginate(10);
    });

    //upload 
    Route::post('upload/image', 'UploadController@upload_image')->name('upload.image');
});
