### What is this? ###
Create a basic setting for laravel project. This repo has :

1. CRUD for User - including activity log

1. Permissions and Roles - can create admin when `migrate --seed`

1. Laravel Collective HTML - verification and validation for form 

1. Medialibrary 7.0 - easy way to upload image

1. Bootstrap 4

1. Font Awesome 5

1. Laracast/Flash - Bootstrap flash message

1. EU Law Cookie Popup 

1. Remove vue from composer

1. Login using email or username *

### Requirement ###
* Composer
* WAMPSERVER (or whatever sql server)
* PHP 7.0+ and Laravel 5.5.0
* MySQL 5.7 or higher is required

### Steps ###
1. Clone .git

1. Run `composer install`

1. Duplicate .env.example and rename to .env

1. Run `php artisan key:generate`

1. Setting database connection inside .env

1. Setting APP_URL inside .env

1. Run `php artisan migrate --seed`

1. Run `npm install`

1. Run `npm run dev` or `npm run production`

1. Run `php artisan storage:link`

1. Run `php artisan serve`

1. Run `php artisan queue:listen`

### Things need to know ###

1. Run `php artisan db:seed --class=PermissionSeeder` to reset permission (in case you add new permission or delete permission)

1. Currently support login and register with email or username. If you want to disable this function, please change coding (read comment inside code) inside LoginController, RegisterController, UserController, User (Model), register.blade, login.blade, user.show.blade, user.edit.blade