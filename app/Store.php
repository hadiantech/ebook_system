<?php

namespace App;



use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public function inventory()
    {
        return $this->hasMany('App\StoreInv','store_id','id');
    }

    public function publisher()
    {
    	return $this->belongsTo('App\Publisher','publisher_id','id');
    }
}

