<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Package extends Model implements HasMedia
{
    use LogsActivity;
    use SoftDeletes;
    use HasMediaTrait;

    protected static $logAttributes = ['title', 'description', 'price', 'started_at', 'ended_at']; 
    protected static $logOnlyDirty = true;
    protected static $logName = 'packages';
    protected static $ignoreChangedAttributes = ['updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    protected $dates = ['deleted_at'];
}
