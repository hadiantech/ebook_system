<?php

namespace App\Http\Controllers;

use App\Publisher;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('publishers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('publishers.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'owner' => 'required',
        ]);

        $publisher = new Publisher;
        $publisher->name = $request->name;
        $publisher->detail = $request->detail;
        $publisher->count = 0;
        $user = User::findOrFail($request->owner);
        $publisher->owner()->associate($user);

        if ( $publisher->save() ) {
            flash('Publisher has been created.');
        } else {
            flash()->error('Unable to create publisher.');
        }

        return redirect()->route('publishers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('publishers.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publisher = Publisher::findOrFail($id);
        return view('publishers.edit', compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publisher $publisher)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
        ]);

        $publisher = Publisher::findOrFail($publisher->id);
        $publisher->name = $request->name;
        $publisher->detail = $request->detail;
        $publisher->save();
        flash()->success('Publisher has been updated.');
        return redirect()->route('publishers.edit', $publisher->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publisher $publisher)
    {
        if( Publisher::findOrFail($publisher->id)->delete() ) {
            flash()->success('Publisher has been deleted');
        } else {
            flash()->success('Publisher not deleted');
        }

        return redirect()->back();
    }
}
