<?php

namespace App\Http\Controllers;
use App\User;
use App\Package;
use Auth;
use Spatie\Activitylog\Models\Activity;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('welcome', compact('packages'));
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function profile()
    {
        $user = Auth::user();
        $logs = Activity::where('causer_id', $user->id)->latest()->limit(100)->get();
        return view('profile.show', compact('user', 'logs'));
    }

    public function profile_edit()
    {
        $user = Auth::user();
        return view('profile.edit', compact('user'));
    }

    public function profile_update(Request $request)
    {
        // Get the user
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $user->id,
        ]);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        $user->save();
        flash()->success('Profile has been updated.');
        return redirect()->route('profile');
    }

    public function profile_edit_pw()
    {
        $user = Auth::user();
        return view('profile.edit_pw', compact('user'));
    }

    public function profile_pw_update(Request $request){
        $this->validate($request, [
            'old_password' => 'required|old_password:' . Auth::user()->password,
            'password' => 'required|confirmed|min:6|different:password_old',
        ]);

        // Get the user
        $user = Auth::user();

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        flash()->success('Password has been updated.');
        return redirect()->route('profile');
    }

}
