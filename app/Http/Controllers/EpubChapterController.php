<?php

namespace App\Http\Controllers;

use App\EpubChapter;
use App\EpubDraft;
use Auth;
use Illuminate\Http\Request;

class EpubChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $draft = EpubDraft::find($id);
        return view('creator.chapter.create', compact('draft'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $draft_id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $count = EpubChapter::where('epub_draft_id', $draft_id)->count();

        $chapter = new EpubChapter;
        $chapter->epub_draft_id  = $draft_id;
        $chapter->title = $request->title;
        $chapter->body = $request->body;
        $chapter->order = $count + 1;

        if ( $chapter->save() ) {
            flash('Chapter has been created.');
        } else {
            flash()->error('Unable to create Chapter.');
        }

        return redirect()->route('creator.show', $draft_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EpubChapter  $epubChapter
     * @return \Illuminate\Http\Response
     */
    public function show(EpubDraft $creator , EpubChapter $chapter)
    {
        return view('creator.chapter.show', compact('creator', 'chapter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EpubChapter  $epubChapter
     * @return \Illuminate\Http\Response
     */
    public function edit(EpubChapter $epubChapter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EpubChapter  $epubChapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EpubDraft $creator , EpubChapter $chapter)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $chapter->title = $request->title;
        $chapter->body = $request->body;

        if ( $chapter->save() ) {
            flash('Chapter has been saved.');
        } else {
            flash()->error('Unable to save Chapter.');
        }

        return redirect()->route('creator.chapter.show', array($creator->id, $chapter->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EpubChapter  $epubChapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(EpubChapter $epubChapter)
    {
        //
    }
}
