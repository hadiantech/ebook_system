<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\EpubPublish;
use App\EpubDraft;
use Auth;

class EpubCreatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
		$drafts = EpubDraft::all();
        return view('creator.index', compact('drafts'));
	}
	
	public function create(){
		//saved all draft before publish
		return view('creator.create');
	}

	public function store(Request $request){
		$this->validate($request, [
            'title' => 'required',
        ]);

        $draft = new EpubDraft;
        $draft->user_id  = Auth::user()->id;
        $draft->title = $request->title;
        $draft->creator = $request->creator;
        $draft->subject = $request->subject;
        $draft->description = $request->description;
        $draft->publisher = $request->publisher;
        $draft->contributor = $request->contributor;
        $draft->published_at = $request->published_at;
        $draft->type = $request->type;
        $draft->format = $request->format;
        $draft->source = $request->source;
        $draft->language = $request->language;
        $draft->relation = $request->relation;
        $draft->coverage = $request->coverage;
        $draft->rights = $request->rights;

        if ( $draft->save() ) {
			$draft->addMediaFromRequest('bookCover')->toMediaCollection('cover');
            flash('Draft has been created.');
        } else {
            flash()->error('Unable to create draft.');
        }

        return redirect()->route('creator.show', $draft->id);
	}

	public function show($id)
    {
		$draft = EpubDraft::find($id);
        return view('creator.show', compact('draft'));
    }

	public function create_page(){

	}

	public function preview(){

	}

	public function publish(EpubDraft $draft){
		EpubPublish::dispatch($draft);
		flash('Epub has been created.');
		return redirect()->route('creator.show', $draft->id);
	}

}
