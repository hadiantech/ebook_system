<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Store;
use App\StoreInv;
use App\User;
class Store_InvController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('store_invs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $store_invs = store_invs::all();
        return view('store_invs.create', compact('store_invs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->validate($request, [
            'price' => 'required',
            'discount' => 'required',
        ]);

        $storeInv = new StoreInv;
        $storeInv->book_id = $request->book_id;
        $storeInv->price = $request->price;
        $storeInv->discount = $request->discount;
        $storeInv->store_id= $request->store_id;
		//suppose to insert publlisher_id if created under publisher


        if ( $storeInv->save() ) {
            flash('Inventory has been registered.');
        } else {
            flash()->error('Unable to register Inventory.');
        }
        return  redirect(url()->previous());
        //return redirect()->route('store_invs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$store_invs=StoreInv::find($id);
        return view('store_invs.show', compact('store_invs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //Ge
        $store_invs=StoreInv::find($id);
           return view('store_invs.show', compact('store_invs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreInv $storeInv)
    {
        //

        $storeInv->price = $request->price;
        $storeInv->discount = $request->discount;
        //suppose to insert publlisher_id if created under publisher
        if ( $storeInv->save() ) {
            flash('Inventory has been updated.');
        } else {
            flash()->error('Unable to update Inventory.');
        }
        return  redirect(url()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreInv  $storeInv
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreInv $storeInv)
    {
        if( $storeInv->delete() ) {

            flash()->success('Inventory has been deleted');
        } else {
            flash()->success('Inventory not deleted');
        }
          return  redirect(url()->previous());
        //return redirect()->back();
    }
}
