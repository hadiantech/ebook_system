<?php

namespace App\Http\Controllers;
use App\User;
use App\Book;
use App\Store;
use App\Package;
use App\Publisher;
use App\Tag;
use Auth;
use Spatie\Activitylog\Models\Activity;

use Illuminate\Http\Request;

class PaymentsController extends Controller
{

    public function index()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('payments.index', compact('books','stores','packages','tags'));
    }

    public function show()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('payments.show', compact('books','stores','packages','tags'));
    }

    public function edit()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('payments.edit', compact('books','stores','packages','tags'));
    }

    public function selectpayment()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('payments.selectpayment', compact('books','stores','packages','tags'));
    }

    public function history()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('payments.history', compact('books','stores','packages','tags'));
    }




}
