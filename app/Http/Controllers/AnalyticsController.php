<?php

namespace App\Http\Controllers;
use App\User;
use App\Book;
use App\Store;
use App\Package;
use App\Publisher;
use App\Tag;
use Auth;
use Spatie\Activitylog\Models\Activity;

use Illuminate\Http\Request;

class AnalyticsController extends Controller
{

    public function index()
    {
        $books = Book::all();
        $stores = Store::all();
        $packages = Package::all();
        $tags = Tag::all();
        return view('analytics.index', compact('books','stores','packages','tags'));
    }




}
