<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\StoreInv;
use App\User;


class StoreController extends Controller
{	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        return view('stores.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = Store::all();
        
        return view('stores.create', compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->validate($request, [
            'name' => 'bail|required|min:2',
        ]);

        $store = new Store;
        $store->store_name = $request->name;
        $store->type = $request->type;
        $store->status = 'Active';

        if  ($request->type =='Private'){
        	 $store->publisher_id = $request->publisher_id;
      	
        }
		//suppose to insert publlisher_id if created under publisher


        if ( $store->save() ) {
            flash('Store has been created.');
        } else {
            flash()->error('Unable to create Store.');
        }

        return redirect()->route('stores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$store = Store::find($id);
    	//$store_inv = $store->inventory->all();
        //return view('stores.show', compact('store','store_inv'));
        return view('stores.show', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
        //Ge
          return view('stores.edit', compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
  
        $store->store_name = $request->name;
        $store->type = $request->type;
        $store->status = 'Active';

        if  ($request->type =='Private'){
        	 $store->publisher_id = $request->publisher_id;
      	
        }
		//suppose to insert publlisher_id if created under publisher


        if ( $store->save() ) {
            flash('Store has been created.');
        } else {
            flash()->error('Unable to create Store.');
        }

        return redirect()->route('stores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        if( $store->delete() ) {
        	//delete all inventori in store
        	$storeInv = StoreInv::where('store_id',$store->id)->delete();
            flash()->success('Store has been deleted');
        } else {
            flash()->success('Store not deleted');
        }

        return redirect()->back();
    }

    public function payout($id)
    {
        $store = Store::find($id);
        return view('stores.payout', compact('store'));
    }
}
