<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();
        return view('packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:2',
            'image' => 'required|image',
            'price' => 'required',
        ]);

        if($request->started_at_null == false){
            $this->validate($request, [
                'started_at' => 'required'
            ]);
        }else{
            $request->started_at = null;
        }

        if($request->ended_at_null == false){
            $this->validate($request, [
                'ended_at' => 'required'
            ]);
        }else{
            $request->ended_at = null;
        }

        $package = new Package;
        $package->title = $request->title;
        $package->description = $request->description;
        $package->price = $request->price;
        $package->started_at = $request->started_at;
        $package->ended_at = $request->ended_at;

        /** *
         * package limit setting. please add to db first before add new one
        */
        $package->books_upload = $request->books_upload;
        $package->books_created = $request->books_created;
        $package->store = $request->store;
        $package->store_inv = $request->store_inv;
        $package->promotion = $request->promotion;
        //** end package limit */

        if ( $package->save() ) {
            $package->addMediaFromRequest('image')->toMediaCollection('image');
            flash('Package has been saved.');
        } else {
            flash()->error('Unable to save package.');
        }

        return redirect()->route('packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        return view('packages.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        return view('packages.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $this->validate($request, [
            'title' => 'required|min:2',
            'price' => 'required',
        ]);

        if($request->started_at_null == false){
            $this->validate($request, [
                'started_at' => 'required'
            ]);
        }else{
            $request->started_at = null;
        }

        if($request->ended_at_null == false){
            $this->validate($request, [
                'ended_at' => 'required'
            ]);
        }else{
            $request->ended_at = null;
        }

        $package->title = $request->title;
        $package->description = $request->description;
        $package->price = $request->price;
        $package->started_at = $request->started_at;
        $package->ended_at = $request->ended_at;

        /** *
         * package limit setting. please add to db first before add new one
        */
        $package->books_upload = $request->books_upload;
        $package->books_created = $request->books_created;
        $package->store = $request->store;
        $package->store_inv = $request->store_inv;
        $package->promotion = $request->promotion;
        //** end package limit */

        if ( $package->save() ) {
            // $package->addMediaFromRequest('image')->toMediaCollection('image');
            flash('Package has been saved.');
        } else {
            flash()->error('Unable to save package.');
        }

        return redirect()->route('packages.edit', $package->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }
}
