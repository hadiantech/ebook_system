<?php

namespace App\Http\Controllers;

use App\Book;
use App\Publisher;
use App\Tag;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all()->pluck('name', 'id')->toArray();
        $languages = ['English','Bahasa Melayu'];
        return view('books.create', compact('tags','languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'epub' => 'required',
            'image' => 'required',
            'publisher_id' => 'required',
            'author' => 'required',
            'amount' => 'required',
        ]);

        $book = new Book;
        $book->name = $request->name;
        $book->detail = $request->detail;
        $book->author = $request->author;
        $book->amount = $request->amount;
        $book->count = 0;
        $publisher = Publisher::findOrFail($request->publisher_id);
        $book->publisher()->associate($publisher);

        if ( $book->save() ) {
            $book->addMediaFromRequest('image')->toMediaCollection('cover');
            $book->addMediaFromRequest('epub')->toMediaCollection('epub');
            $book->tags()->attach($request->tags);
            
            flash('Book has been saved.');
        } else {
            flash()->error('Unable to save book.');
        }

        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $tags = Tag::all()->pluck('name', 'id')->toArray();
        return view('books.show', compact('book','tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        
        $book->fill($request->all);

        $book->save();
        flash()->success('Book has been updated.');
        //return redirect()->route('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        if( $book->delete() ) {
            flash()->success('Book has been deleted');
        } else {
            flash()->success('Book not deleted');
        }

        return redirect()->back();
    }
}
