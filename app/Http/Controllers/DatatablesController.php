<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\User;
use App\Tag;
use App\Publisher;
use App\Book;
use App\Store;
use App\StoreInv;

class DatatablesController extends Controller
{
    public function getUser(){
        return Datatables::eloquent(User::with('roles'))
        ->addColumn('action', function($model){
            $url = 'users';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addColumn('roles', function($model){
            return $model->roles->map(function($roles) {
                return $roles->name;
            })->implode(', ');
        })
        ->addIndexColumn()
        ->make(true);
    }
    
    public function getTag(){
        return Datatables::eloquent(Tag::query())
        ->addColumn('action', function($model){
            $url = 'tags';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getPublisher(){

        return Datatables::of(Publisher::with('owner')->select('publishers.*'))
        ->addColumn('action', function($model){
            $url = 'publishers';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getBook(){

        return Datatables::of(Book::with('publisher')->select('books.*'))
        ->addColumn('action', function($model){
            $url = 'books';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getStore(){

        return Datatables::eloquent(Store::query())
        ->addColumn('action', function($model){
            $url = 'stores';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

     public function getStoreInv($id){

        return Datatables::eloquent(StoreInv::with('item_info')->select('*')->where('store_invs.store_id',$id))
        ->addColumn('action', function($model){
            $url = 'storeInvs';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }
}
