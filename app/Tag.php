<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use LogsActivity;
    use SoftDeletes;

    protected static $logAttributes = ['name']; 
    protected static $logOnlyDirty = true;
    protected static $logName = 'tags';
    protected static $ignoreChangedAttributes = ['updated_at', 'count'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    protected $dates = ['deleted_at'];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
