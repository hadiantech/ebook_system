<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Permission  extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_posts',
            'add_posts',
            'edit_posts',
            'delete_posts',

            'view_tags',
            'add_tags',
            'edit_tags',
            'delete_tags',
            
            'view_publishers',
            'add_publishers',
            'edit_publishers',
            'delete_publishers',

            'view_books',
            'add_books',
            'edit_books',
            'delete_books',

            'view_stores',
            'add_stores',
            'edit_stores',
            'delete_stores',

            'view_storeInvs',
            'add_storeInvs',
            'edit_storeInvs',
            'delete_storeInvs',

            'view_packages',
            'add_packages',
            'edit_packages',
            'delete_packages',
        ];
    }
}
