<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Classes\Epub;
use App\Classes\Zip;
use App\Classes\EpubPacker;
use App\Classes\EpubCore;

use App\EpubDraft;

class EpubPublish implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $draft;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(EpubDraft $draft)
    {
        $this->draft = $draft;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $draft = $this->draft;

        $bookInfo = array(
			'title'       => $draft->title,
			'creator'     => $draft->creator,
			'subject'     => $draft->subject,
			'description' => $draft->description,
			'publisher'   => $draft->publisher,
			'contributor' => $draft->contributor,
			'date'        => $draft->published_at,
			'type'        => $draft->type,
			'format'      => $draft->format,
			'source'      => $draft->source,
			'language'    => $draft->language,
			'relation'    => $draft->relation,
			'coverage'    => $draft->coverage,
			'rights'      => $draft->rights
		);

		$epub = new EpubPacker('./public/epub/'. $draft->title);
		$epub->init();
		$epub->setBookInfo($bookInfo);
		$epub->makeCover($draft->getMedia('cover')->first()->getFullUrl());

		$imgLocation = './public/';

		$i = 1;

		foreach( $draft->chapters as $chapter ){
			$chapterBody = $epub->bodyFilter($chapter->body ,$imgLocation);
			$epub->setData($chapter->title ,$chapter->body ,$i);
			$chapterFileName = $epub->saveChapter();
			if($chapterFileName){
				$epub->addChapter();
				$i++;
			}
		}

		$pack = $epub->saveBook('./public/storage/epub/' , $draft->title.'.epub');
    }
}
