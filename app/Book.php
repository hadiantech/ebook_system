<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Book extends Model implements HasMedia
{
    use LogsActivity;
    use SoftDeletes;
    use HasMediaTrait;

    protected static $logAttributes = ['name', 'detail', 'author', 'amount']; 
    protected static $logOnlyDirty = true;
    protected static $logName = 'books';
    protected static $ignoreChangedAttributes = ['updated_at', 'count'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'count', 'detail', 'author', 'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    protected $dates = ['deleted_at'];

    public function publisher()
    {
        return $this->belongsTo(Publisher::class, 'publisher_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
