<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;
    use HasMediaTrait;

    protected static $logAttributes = ['name', 'email', 'password', 'username']; //disable if dont want allow username as auth
    // protected static $logAttributes = ['name', 'email', 'password'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'users';
    protected static $ignoreChangedAttributes = ['updated_at', 'remember_token'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',//disable if dont want allow username as auth
        // 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function publisher()
    {
        return $this->hasOne(Publisher::class, 'user_id');
    }
}
