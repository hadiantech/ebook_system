<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class EpubDraft extends Model implements HasMedia
{
    use LogsActivity;
    use SoftDeletes;
    use HasMediaTrait;

    protected static $logAttributes = ["*"]; 
    protected static $logOnlyDirty = true;
    protected static $logName = 'epub_draft';
    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    protected $dates = ['deleted_at'];

    public function chapters()
    {
        return $this->hasMany(EpubChapter::class);
    }

}
