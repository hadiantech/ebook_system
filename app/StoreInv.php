<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreInv extends Model
{
    //
      protected $table='store_invs';

      public function store()
    {
        return $this->belongsTo('App\Store', 'store_id');

    }

    public function item_info()
    {
        return $this->belongsTo('App\Book', 'book_id');
    }

}
