<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use LogsActivity;
    use SoftDeletes;

    protected static $logAttributes = ['name', 'detail']; 
    protected static $logOnlyDirty = true;
    protected static $logName = 'publishers';
    protected static $ignoreChangedAttributes = ['updated_at', 'count'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'count', 'detail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];

    protected $dates = ['deleted_at'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
