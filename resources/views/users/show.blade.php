@extends('layouts.dashboard.master') @section('title', 'User Management') @section('subtitle', 'View') @section('content')

<div class='col-lg-12 bg-white border p-3 '>
    <table class="table table-bordered table-striped">
        <tr>
            <td>Name</td>
            <td>{{$user->name}}</td>
        </tr>
        {{-- disable if dont want allow username as auth --}}
        <tr>
            <td>Username</td>
            <td>{{$user->username}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <td>Roles</td>
            <td>
                @foreach($user->roles as$role) {{$role->name}} <br> @endforeach
            </td>
        </tr>
    </table>

    <a class="btn btn-link" href="{{ route('users.index') }}">Back</a>
    <a class="btn btn-info" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">View Activity Log</a> @can('edit_users')
    <a class="btn btn-warning" href="{{route('users.edit', $user->id)}}">Edit</a>
    <a class="btn btn-warning" href="{{route('users.edit_pw', $user->id)}}">Change Password</a> @endcan @can('delete_users')
    <a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route('users.destroy', $user->id) }}">Delete</a> @endcan


</div>

 
<div class="collapse col-lg-12 px-0 py-4" id="collapseExample">
    <div class='col-lg-12 bg-white'>
        <hr />
        <h3 class="my-3">Log Activity</h3>
        <table class="table table-bordered py-3">
            <tr>
                <td>Time</td>
                <td>Model Changes</td>
                <td>Description</td>
                <td>Model ID</td>
            </tr>
            @foreach($logs as $log)
            <tr>
                <td>
                    {{$log->created_at}}
                </td>
                <td>
                    {{$log->subject_type}}
                </td>
                <td>
                    {{$log->description}}
                </td>
                <td>
                    {{$log->subject_id}}
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>


@endsection
