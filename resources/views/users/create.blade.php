@extends('layouts.dashboard.master') 
@section('title', 'Create New User') 
@section('subtitle', 'Create') 
@section('content')

<div class="col-lg-12 card p-4">

    {{ Form::open(array('url' => 'users')) }}
    
    <div class="form-group">
        {{ Form::label('email', 'E-Mail Address') }} {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
    </div>

    {{-- disable if dont want allow username as auth --}}
    <div class="form-group">
        {{ Form::label('username', 'Username') }} {{ Form::text('username', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }} {{ Form::password('password', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirm Password') }} {{ Form::password('password_confirmation', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('roles_label', 'Roles') }} @foreach($roles as $role)
        <br /> {{ Form::checkbox('roles[]', $role->id) }} {{ Form::label('roles', $role->name) }} @endforeach
    </div>

    {{ Form::submit('Add', array('class' => 'btn btn-success', 'style' => 'width:130px')) }} {{ Form::close() }}

</div>


@endsection
