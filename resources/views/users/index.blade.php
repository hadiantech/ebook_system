@extends('layouts.dashboard.master') @section('title', 'User') @section('content')
<div class="row no-gutters">
    <div class="col-sm-12 my-3">
        <div class="mb-3">
            <div class="d-flex flex-row-reverse">
                @can('add_users')
                <a href="{{route('users.create')}}" class="btn btn-success">
                    <i class="fas fa-plus mr-2"></i> Add New User
                </a>
                @endcan
            </div>
        </div>
    </div>
    <div class="col-sm-12 card p-4">
        <table id="users-table" class="table table-bordered table-hover">
            <thead class="">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection @section('js') @parent

<script>
    $(function() {
        $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/datatables/getUser',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name'
                },
                {
                    data: 'email'
                },
                {
                    data: 'roles',
                    name: 'roles.name',
                    orderable: false
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
