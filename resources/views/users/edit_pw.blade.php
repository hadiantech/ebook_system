@extends('layouts.dashboard.master') @section('title', 'User Management') @section('subtitle', 'Edit Password') @section('content')


<div class='col-lg-12 p-4 border bg-white'>
    {{ Form::model($user, ['route' => ['users.update_pw', $user->id], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('password', 'Password') }} {{ Form::password('password', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirm Password') }} {{ Form::password('password_confirmation', array('class' => 'form-control', 'required')) }}
    </div>

    <a class="btn btn-link" href="{{ url()->previous() }}">Back</a> {{ Form::submit('Update', array('class' => 'btn btn-success')) }} {{ Form::close() }}

</div>


@endsection
