<div class="col-sm-12 bg-white p-4 text-center">
    <i style="font-size:3em" class="fas fa-clipboard-check p-3 mb-3 text-green"></i>
    <p>Once you have a book ready to publish, there will be short delay due to processing queue. </p>
    {{ Form::submit('Publish', array('class' => 'btn btn-success','style' => 'width:150px')) }} 
</div>