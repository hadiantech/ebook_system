<div class="form-group">
    {{ Form::label('name', 'Title') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('name', 'Sub-title') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('detail', 'Description') }} {{ Form::textarea('detail', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('author', 'Author') }} {{ Form::text('author', null, array('class' => 'form-control', 'required')) }}
</div>
<div class="form-group">
    {{ Form::label('publisher_id', 'Publisher') }}
    <select id="publisher_id" name="publisher_id" class="select2 form-control" required></select>
</div>


<div class="form-group">
    {{ Form::label('tags', 'Tag/Category') }}
    {{ Form::select('tags', $tags, null, ['multiple', 'required', 'class' => 'form-control', 'name'=>'tags[]']) }}
</div>

{{-- <div class="form-group">
    {{ Form::label('language', 'Language') }}
    {{ Form::select('language', $languages, null, ['multiple', 'required', 'class' => 'form-control', 'name'=>'language']) }}
</div> --}}

<div class="form-group">
    {{ Form::label('pageCount', 'Page Count') }} 
    {{ Form::text('pageCount', null, array('class' => 'form-control', 'required')) }}
</div>

<div class="form-group">
    {{ Form::label('onSaleDate', 'On Sale Date') }} 
    {{ Form::text('onSaleDate', null, array('class' => 'form-control col-lg-3', 'required', 'id' => 'datepicker1')) }}
</div>

<div class="form-group">
    {{ Form::label('publicationDate', 'Publicaton Date') }} 
    {{ Form::text('publicationDate', null, array('class' => 'form-control col-lg-3', 'required', 'id' => 'datepicker2')) }}
</div>

<div class="form-group d-flex flex-row-reverse">
    <a href="javascript:;" id="next_content" class="btn btn-success">Next > Upload Content</a>
</div>