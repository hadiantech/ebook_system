<div class="row mb-4">
    <div class="col-md-3">
        <div class="bg-cover rounded" style="min-height:280px; width:100%; background-image:url({{ $book->getMedia('cover')->first()->getUrl() }})"></div>
    </div>
    <div class="col-md-6">
        <p class="mb-1">{{ $book->name }}</p>
        <p class="mb-0" style="">By {{ $book->author }}</p>
        <p class="mb-3" style="">Total Downloads: 10</p>
        <a href="{{ $book->getMedia('epub')->first()->getUrl()}}">Download</a>
    </div>
</div>

<div class="form-group">
    {{ Form::label('epub', 'Upload New ePub File') }}
    <div class="input-group mb-3">
        <div class="custom-file" style="">
            {{ Form::file('epub' ,['class' => "custom-file-input", 'id' => "inputGroupFile01", 'accept'=>".epub"]) }}
            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
        </div>
    </div>
</div>
<div class="form-group">
    {{ Form::label('image', 'Upload New Cover Image') }}
    <div class="input-group mb-3">
        <div class="custom-file">
            {{ Form::file('image' ,['class' => "custom-file-input", 'id' => "inputGroupFile02", 'accept'=>"image/*"]) }}
            <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
        </div>
    </div>
</div>