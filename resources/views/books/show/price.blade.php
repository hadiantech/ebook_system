<div class="form-group">
    {{ Form::label('amount', 'Amount') }}
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">RM</span>
        </div>{{ Form::text('amount', null, array('class' => 'form-control', 'required')) }}
    </div>
</div>
