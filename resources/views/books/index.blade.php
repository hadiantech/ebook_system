@extends('layouts.dashboard.master')
@section('title', 'BOOK CATALOGUE')
@section('subtitle', 'All Book')
@section('content')

<div class="col-sm-12 p-0">
    <div class="">
        <div class="d-flex">
            @can('add_books')
            <div class="col-md-6 m-3 border rounded bg-white">
                <div class="p-3 m-2">
                    <a href="{{ route('books.create') }}" class="btn btn-success">
                        <i class="fas fa-plus mr-2"></i> UPLOAD NEW BOOK
                    </a>
                    <p class="m-0 mt-2 p-0">Create a new book by uploading existing file.
                    </p>
                </div>
            </div>
            <div class="col-md-6 m-3 border rounded bg-white">
                <div class="p-3 m-2">
                    <a href="{{ route('creator.create') }}" class="btn btn-success">
                        <i class="fas fa-plus mr-2"></i> CREATE NEW BOOK
                    </a>
                    <p class="m-0 mt-2 p-0">Create a new book using our LeafBook ePub Creator.</p>
                </div>
            </div>
            @endcan
        </div>
    </div>
</div>


@foreach ($books as $b)
<div class="col-md-3 p-0">
    <div class="p-3 m-3 bg-white rounded">
        <div class="bg-cover" style="min-height:280px; width:100%; background-image:url({{ $b->getMedia('cover')->first()->getUrl() }})"></div>
        <p class="mt-3 mb-1">{{ $b->name }}</p>
        <p class="mb-0" style="font-size:13px">By {{ $b->author }}</p>
        <p class="mb-3" style="font-size:13px">Total Downloads: 10</p>
    </div>
</div>
@endforeach


<div class="col-sm-12 mt-3">
    <div class="p-4 bg-white border">
        <table id="books-table" class="table table-bordered table-hover">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Publisher</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    
</div>
@endsection 
@section('js') 
@parent

<script>
    $(function() {
        $('#books-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getBook',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name',
                    name: 'books.name'
                },
                {
                    data: 'publisher.name',
                    name: 'publisher.name'
                },
                {
                    data: 'created_at',
                    name: 'books.created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
