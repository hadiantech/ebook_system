<div class="form-group">
    
    <div class="input-group mb-3">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="customCheck1" checked disabled>
            <label class="custom-control-label" for="customCheck1">DRM Protection (BETA)</label>
            <p class="mt-2">LeafBook DRM protection is enabled to all uploaded books.</p>
        </div>
    </div>
</div>
<div class="form-group d-flex flex-row-reverse">
    <a href="javascript:;" id="next_publish" class="btn btn-success">Next > Publish</a>
</div>