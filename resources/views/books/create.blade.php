@extends('layouts.dashboard.master') 
@section('title', 'Upload New Book') 
@section('subtitle', 'Create') 
@section('content')

{{ Form::open(['url' => 'books', 'files' => 'true']) }}

<div class="row bg-white rounded">
    <div class="col-md-12 p-0 m-0">
        <div class="nav nav-pills nav-fill upload-tab rounded-top p-0" id="nav-tab" role="tablist">
            <a class="nav-item nav-link newscycle bold active" id="nav-metadata-tab" data-toggle="tab" href="#nav-metadata" role="tab" aria-controls="nav-metadata" aria-selected="true">Metadata</a>
            <a class="nav-item nav-link newscycle bold" id="nav-content-tab" data-toggle="tab" href="#nav-content" role="tab" aria-controls="nav-content" aria-selected="false">Content</a>
            <a class="nav-item nav-link newscycle bold" id="nav-price-tab" data-toggle="tab" href="#nav-price" role="tab" aria-controls="nav-price" aria-selected="false">Price</a>
            <a class="nav-item nav-link newscycle bold" id="nav-settings-tab" data-toggle="tab" href="#nav-settings" role="tab" aria-controls="nav-settings" aria-selected="false">Settings</a>
            <a class="nav-item nav-link newscycle bold" id="nav-publish-tab" data-toggle="tab" href="#nav-publish" role="tab" aria-controls="nav-publish" aria-selected="false">Publish</a>
        </div>
    </div>
    
    <div class="tab-content rounded-bottom" id="nav-tabContent" style="min-width:800px">
        <div class="tab-pane fade show active border-top p-4" id="nav-metadata" role="tabpanel" aria-labelledby="nav-metadata-tab">
            @include('books.metadata')
        </div>
        <div class="tab-pane fade border-top p-4" id="nav-content" role="tabpanel" aria-labelledby="nav-content-tab">
            @include('books.content')
        </div>
        <div class="tab-pane fade border-top p-4" id="nav-price" role="tabpanel" aria-labelledby="nav-price-tab">
            @include('books.price')
        </div>
        <div class="tab-pane fade border-top p-4" id="nav-settings" role="tabpanel" aria-labelledby="nav-setting-tab">
            @include('books.setting')
        </div>
        <div class="tab-pane fade border-top p-4" id="nav-publish" role="tabpanel" aria-labelledby="nav-publish-tab">
            @include('books.publish')
        </div>
    </div>
</div>
{{ Form::close() }}

@endsection 
@section('js') 
@parent
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $('#datepicker1').datepicker({
        uiLibrary: 'bootstrap4'
    });

     $('#datepicker2').datepicker({
        uiLibrary: 'bootstrap4'
    });
</script>

<script>
    $('#next_content').on('click', function (e) {
        $('#nav-tab a[href="#nav-content"]').tab('show')
    })
    $('#next_price').on('click', function (e) {
        $('#nav-tab a[href="#nav-price"]').tab('show')
    })
    $('#next_setting').on('click', function (e) {
        $('#nav-tab a[href="#nav-settings"]').tab('show')
    })
    $('#next_publish').on('click', function (e) {
        $('#nav-tab a[href="#nav-publish"]').tab('show')
    })
</script>


<script>
    $(document).ready(function() {
        $('#tags').select2({
            theme: "bootstrap"
        });

        $('#language').select2({
            theme: "bootstrap"
        });

        $('#publisher_id').select2({
            theme: "bootstrap",
            ajax: {
                url: '/select2/getBook',
                dataType: 'json',
                delay: 200,
                data: function(params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }
            },
            minimumInputLength: 1,
            templateResult: function(repo) {
                if (repo.loading) return repo.name;
                var markup = repo.name;
                return markup;
            },
            templateSelection: function(repo) {
                return repo.name;
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });

    });

    $('#inputGroupFile01').on('change', function() {
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#inputGroupFile02').on('change', function() {
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })

</script>
@endsection
