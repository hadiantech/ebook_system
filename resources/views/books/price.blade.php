<div class="form-group">
    {{ Form::label('amount', 'Amount') }}
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">RM</span>
        </div>{{ Form::text('amount', null, array('class' => 'form-control', 'required')) }}
    </div>
</div>
<div class="form-group d-flex flex-row-reverse">
    <a href="javascript:;" id="next_setting" class="btn btn-success">Next > Setting</a>
</div>