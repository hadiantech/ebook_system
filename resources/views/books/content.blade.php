<div class="form-group">
    {{ Form::label('epub', 'Upload ePub file') }}
    <div class="input-group mb-3">
        <div class="custom-file" style="">
            {{ Form::file('epub' ,['class' => "custom-file-input", 'id' => "inputGroupFile01", 'accept'=>".epub", 'required']) }}
            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
        </div>
    </div>
</div>
<div class="form-group">
    {{ Form::label('image', 'Cover Image') }}
    <div class="input-group mb-3">
        <div class="custom-file">
            {{ Form::file('image' ,['class' => "custom-file-input", 'id' => "inputGroupFile02", 'accept'=>"image/*", 'required']) }}
            <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
        </div>
    </div>
</div>
<div class="form-group d-flex flex-row-reverse">
    <a href="javascript:;" id="next_price" class="btn btn-success">Next > Setup Price</a>
</div>