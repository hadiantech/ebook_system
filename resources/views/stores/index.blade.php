@extends('layouts.dashboard.master') 
@section('title', 'Store Management')
@section('subtitle', 'All Stores')
@section('content')

<div class="col-sm-12 mb-4 p-0">
    <div class="">
        <div class="d-flex flex-row-reverse">
            @can('add_stores')
            <a href="{{ route('stores.create') }}" class="btn btn-success">
                <i class="fas fa-plus mr-2"></i> New
            </a> 
            @endcan
        </div>
    </div>
</div>


<div class="col-sm-12">
    <div class="row">
        @foreach ($stores as $s)
        <div class="col-sm-12 p-0">
            <div class="border my-2 rounded bg-white p-5 border-success">
                <div class="row">
                    <div class="col-sm-3">
                        <p class="mb-0">Store Name</p>
                        <h3 class="oswald thin mb-1">{{ $s->store_name }}</h3>
                    </div>
                    <div class="col-sm-2">
                        <p class="mb-0">Type</p>
                        <h3 class="oswald thin mb-1">{{ $s->type }}</h3>
                    </div>
                    <div class="col-sm-2">
                        <p class="mb-0">Status</p>
                        <h3 class="oswald thin mb-1">{{ $s->status }}</h3>
                    </div>
                    <div class="col-sm-2">
                        <p class="mb-0">Books</p>
                        <h3 class="oswald thin mb-1">43</h3>
                    </div>
                    <div class="col-sm-2">
                        <p class="mb-0">Total Sales</p>
                        <h3 class="oswald thin mb-1">RM3,300.30</h3>
                    </div>
                    <div class="col-sm-1 align-self-center ml-auto">
                        <a href="{{ route('stores.show',$s->id) }}" title="Manage" class="text-green"><i class="fas fa-chevron-circle-right" style="font-size:2em;"></i></a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>



<div class="col-sm-12 card p-4 mt-1">
    <table id="stores-table" class="table table-bordered table-hover">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@endsection 
@section('js') 
@parent

<script>
    $(function() {
        $('#stores-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getStore',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'store_name',
                    name: 'stores.name'
                },
                {
                    data: 'type',
                    name: 'stores.type'
                },
                {
                    data: 'status',
                    name: 'stores.status'
                },
                {
                    data: 'created_at',
                    name: 'stores.created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
