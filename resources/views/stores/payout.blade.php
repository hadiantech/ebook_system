@extends('layouts.dashboard.master') 
@section('title', 'Payout') 
@section('subtitle', 'View') 
@section('content')

<div class="col-lg-12">
    <div class="card text-white bg-success mb-3">
        <div class="card-header">
            <h4 class="oswald thin mb-1">July</h4>
        </div>
        <div class="card-body bg-white text-black p-5">
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-md-12 mt-4 m-0 p-0">
                    <div class="bg-white">
                        <canvas id="myChart-week" style="width:100%; height:300px;"></canvas>
                        <canvas id="myChart-month" style="width:100%; height:300px;"></canvas>
                        <canvas id="myChart-year" style="width:100%; height:300px;"></canvas>
                        
                    </div>
                    <div class="p-3">
                        <button id="weekbtn" class="btn btn-orange">Week</button>
                        <button id="monthbtn" class="btn btn-orange" disabled>Month</button>
                        <button  id="yearbtn" class="btn btn-orange">Year</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
    data = [
                { x: "01/01/2018", y: 43}, 
                { x: "02/01/2018", y: 12}, 
                { x: "03/01/2018", y: 55}, 
                { x: "04/01/2018", y: 67}, 
                { x: "05/01/2018", y: 120}, 
                { x: "06/01/2018", y: 33}, 
                { x: "07/01/2018", y: 45}, 
                { x: "08/01/2018", y: 40}, 
                { x: "09/01/2018", y: 12}, 
                { x: "10/01/2018", y: 343},
                { x: "11/01/2018", y: 343},
                { x: "12/01/2018", y: 343}
            ];
    rendergraph(data);
    $( "#myChart-week" ).css('display','none');
    $( "#myChart-month" ).css('display','block');
    $( "#myChart-year" ).css('display','none');

    function rendergraph(data){
        var ctx = document.getElementById("myChart-month");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: 'Total Books Sold',
                    data: data,
                    backgroundColor:'rgba(18,172,82,0.6)',
                    borderColor:'#02522A',
                    borderWidth: 1,
                    fill: 1,
                    lineTension: 0
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                    type: "time",
                    time: {
                        unit: 'month',
                        round: 'month',
                        displayFormats: {
                            month: 'MMM YY',
                            day: 'd',
                            year: 'YYYY'
                            }
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                }
            }
        });
    }
    

    $( "#weekbtn" ).click(function() {
        data = [
                { x: "01/01/2018", y: 43}, 
                { x: "02/01/2018", y: 11}, 
                { x: "03/01/2018", y: 545}, 
                { x: "04/01/2018", y: 627}, 
                { x: "05/01/2018", y: 1230}, 
                { x: "06/01/2018", y: 313}, 
                { x: "07/01/2018", y: 425}, 
                { x: "08/01/2018", y: 440}, 
                { x: "09/01/2018", y: 212}, 
                { x: "10/01/2018", y: 4343},
                { x: "11/01/2018", y: 343},
                { x: "12/01/2018", y: 343}
            ];

        var ctx = document.getElementById("myChart-week");
        var myChartw = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: 'Total Books Sold',
                    data: data,
                    backgroundColor:'rgba(18,172,82,0.6)',
                    borderColor:'#02522A',
                    borderWidth: 1,
                    fill: 1,
                    lineTension: 0
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                    type: "time",
                    time: {
                        unit: 'month',
                        round: 'month',
                        displayFormats: {
                            month: 'MMM YY',
                            day: 'd',
                            year: 'YYYY',
                            max: '07/29/2018'
                            }
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                }
            }
        });

        $( "#weekbtn" ).attr('disabled',true);
        $( "#weekbtn" ).siblings().attr('disabled',false);
        $( "#myChart-week" ).css('display','block')
        $( "#myChart-week" ).siblings().css('display','none')
    
        // myChart.data.datasets[0].data = data;
        // myChart.update();
    
    });

    $( "#monthbtn" ).click(function() {
        
        data = [
                { x: "01/01/2018", y: 43},
                { x: "02/01/2018", y: 11},
                { x: "03/01/2018", y: 545},
                { x: "04/01/2018", y: 627},
                { x: "05/01/2018", y: 1230},
                { x: "06/01/2018", y: 313},
                { x: "07/01/2018", y: 425},
                { x: "08/01/2018", y: 440},
                { x: "09/01/2018", y: 212},
                { x: "10/01/2018", y: 4343},
                { x: "11/01/2018", y: 343},
                { x: "12/01/2018", y: 343}
            ];
        
        var ctx = document.getElementById("myChart-month");
        var myChartm = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: 'Total Books Sold',
                    data: data,
                    backgroundColor:'rgba(18,172,82,0.6)',
                    borderColor:'#02522A',
                    borderWidth: 1,
                    fill: 1,
                    lineTension: 0
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                    type: "time",
                    time: {
                        unit: 'month',
                        round: 'month',
                        displayFormats: {
                            month: 'MMM YY',
                            day: 'd',
                            year: 'YYYY'
                            }
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                }
            }
        });
    
        $( "#monthbtn" ).attr('disabled',true);
        $( "#monthbtn" ).siblings().attr('disabled',false);
        $( "#myChart-month" ).css('display','block')
        $( "#myChart-month" ).siblings().css('display','none')
        
    });

    $( "#yearbtn" ).click(function() {
        
        label = ["2016","2017","2018","2019"];
        data = [0,0,123,0];
        $( "#yearbtn" ).attr('disabled',true);
        $( "#yearbtn" ).siblings().attr('disabled',false);
        //rendergraph(data,label);
    });




</script>
@endsection