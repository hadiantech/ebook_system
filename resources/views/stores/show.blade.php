@extends('layouts.dashboard.master') 
@section('title', 'Store Management') 
@section('subtitle', 'View') 
@section('content')

<div class="col-sm-12">
    <div class="card text-white bg-success mb-3">
        <div class="card-body bg-white text-black p-5">
            <div class="row">
                <div class="col-sm-2">
                    <p class="mb-0">Store Name</p>
                    <h3 class="oswald thin mb-1">{{ $store->store_name }}</h3>
                </div>
                <div class="col-sm-2">
                    <p class="mb-0">Type</p>
                    <h3 class="oswald thin mb-1">{{ $store->type }}</h3>
                </div>
                <div class="col-sm-2">
                    <p class="mb-0">Status</p>
                    <h3 class="oswald thin mb-1">{{ $store->status }}</h3>
                </div>
                <div class="col-sm-3">
                    <p class="mb-0">Created at</p>
                    <h3 class="oswald thin mb-1">{{ \Carbon\Carbon::parse($store->created_at)->format('d M Y')}}</h3>
                </div>
                <div class="col-sm-3">
                    <p class="mb-0">Last updated at</p>
                    <h3 class="oswald thin mb-1">{{ \Carbon\Carbon::parse($store->updated_at)->format('d M Y')}}</h3>
                </div>
            </div>
            
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="card text-white bg-success mb-3">
        <div class="card-header">
            <h4 class="oswald thin mb-1">Sales</h4>
        </div>
        <div class="card-body bg-white text-black p-5">
            <div class="row">
                <div class="col-sm-3 border border-warning p-3 mr-2 text-center">
                    <p class="mb-0">Last month</p>
                    <h3 class="oswald thin mb-1">RM35.00</h3>
                </div>
                <div class="col-sm-3 border border-warning p-3 mr-2 text-center">
                    <p class="mb-0">This month</p>
                    <h3 class="oswald thin mb-1">RM113.00</h3>
                </div>
                <div class="col-sm-3 border border-warning bg-warning p-3 mr-2 text-center">
                    <p class="mb-0 bold">Total</p>
                    <h3 class="oswald thin mb-1">RM148.00</h3>
                </div>
                <div class="col-sm-2 mr-2 text-center d-flex align-items-end justify-content-center">
                    <a href="{{ url()->current() }}/payout" class="btn btn-success">View Payout <i class="far fa-arrow-alt-circle-right ml-2"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-4 m-0 p-0">
                    <div class="bg-white">
                        <canvas id="myChart" style="width:100%; height:300px;"></canvas>
                        <div class="p-3">
                            <button id="weekbtn" class="btn btn-orange">Week</button>
                            <button id="monthbtn" class="btn btn-orange" disabled>Month</button>
                            <button  id="yearbtn" class="btn btn-orange">Year</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-12">
    <div class="card text-white bg-success mb-3">
        <div class="card-header">
            <h4 class="oswald thin mb-1">Store Inventory</h4>
        </div>
        <div class="card-body bg-white text-black p-5">
            <div class="row">
                <div class="col-sm-12 mb-4 p-0">
                    <div class="">
                        <div class="d-flex flex-row-reverse">
                            @can('add_stores')
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addInventoryModal"> <i class="fas fa-plus"></i> Add Inventory </button>
                            @endcan 
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <p class="mb-0">Books</p>
                    <h3 class="oswald thin mb-1">43</h3>
                </div>
                <div class="col-sm-12 card p-4">
                    <table id="inv-table" class="table table-bordered table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th>#</th>
                                <th>Book Name</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
 

<!--- modal part terpaksa gabung-->
<div class="modal fade" id="addInventoryModal" tabindex="-1" role="dialog" aria-labelledby="AddInventory" aria-hidden="true">
    {{ Form::open(['url' => 'storeInvs', 'files' => 'true']) }}
        <div class="modal-dialog modal-lg p-5" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Inventory</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('select_book', 'Select Book') }} 
                        <select  name='book_id' id='book_id' ></select>
                    </div>
                    <div class="form-group book_preview">
                        <table class="table table-bordered table-striped small">
                            <tr>
                                <td id='book_image' width="150px" rowspan="4"></td>
                                <td>Name</td>
                                <td id='book_name'></td>
                            </tr>
                            <tr>
                                <td>Author</td>
                                <td id='book_author' ></td>
                            </tr>
                            <tr>
                                <td>Detail</td>
                                <td id='book_detail'></td> 
                            </tr>
                        </table>
                    </div>
                    <div class="form-group">
                        {{ Form::label('lbl_price', 'Price') }} {{ Form::number('price', '0.00', array('class' => 'form-control', 'required' ,'id' => 'price','step'=>'.01')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('lbl_discount', 'Discount (%)') }} {{ Form::number('discount', '0.00', array('class' => 'form-control', 'required' ,'id' => 'discount','step'=>'.01' )) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('lbl_total', 'Total Price') }} {{ Form::text('total', '0.00', array('class' => 'form-control', 'readonly' ,'id' => 'total')) }}
                    </div>
                    <br />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    {{ Form::hidden('store_id', $store->id,array('id' => 'store_id')) }}
                    {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}     
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
@section('js')
@parent
    <script src="https://cdn.jsdelivr.net/epub.js/0.2.15/epub.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $(document).ready(function(){

        $('#book_id').select2({
            theme: "bootstrap",
            dropdownParent: $("#addInventoryModal"),
            ajax: {
                url: '/select2/getNotInStoreBook',
                dataType: 'json',
                delay: 200,
                data: function(params) {

                    var query = {
                        q: params.term,
                        page: params.page,
                        store_id: $('#store_id').val(),
                    };

                return query;
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }

            
            },
            templateResult: function(repo) {
                if (repo.loading) return repo.name;
                var markup = repo.name;
                return markup;
            },
            templateSelection: function(repo) {
                $.getJSON("{{ url('ajax/getBookByID')}}", 
                    { book_id: repo.id }, 
                    function(data) {
                    $('#book_name').html(data.book.name);
                    $('#book_detail').html(data.book.detail);
                    $('#book_author').html(data.book.author);
                    $('#book_image').html( '<img class="card-img-top" src="'+data.image+'">');
                        //alert(data.id);
                    });
                return repo.name;
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        // $('#book_id').change(function(){
        //     $.getJSON("{{ url('select/getNotInStoreBook')}}", 
        //         { store_id: $('#store_id').val() }, 
        //         function(data) {
        //             var model = $('#book_id');
        //             model.empty();

                
        //             $.each(data, function(index,element) {
        //                 model.append("<option value='"+element.id+"'>" + element.name + "</option>");
        //             });
        //         });
        // });
        //  $('#book_id').change();

  

        $('#inv-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getStoreInv/{{$store->id}}',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'item_info.name',
                    name: 'inv.book_id'
                },
                {
                    data: 'price',
                    name: 'inv.price'
                },
                {
                    data: 'discount',
                    name: 'inv.discount'
                },
                {
                    data: 'created_at',
                    name: 'inv.created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

    
        $('#price').keyup(function(){
            calculateTotal();
        });

        $('#discount').keyup(function(){
            calculateTotal();
        });

  });

    function calculateTotal(){
        price= $('#price').val();
        discount =$('#discount').val();
        total =(price-(price*(discount/100))).toFixed(2);
        $('#total').val( total);
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>

    label = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE","JUL","AUG","SEP","OCT","NOV","DEC"];
    data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0];
    rendergraph(data,label)

    $( "#weekbtn" ).click(function() {
        label = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"];
        data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0,12, 19, 3, 5, 2, 3,0,0,0,0,0,0,4,5,6,7,8,9];
        $( "#weekbtn" ).attr('disabled',true);
        $( "#weekbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    
    });

    $( "#monthbtn" ).click(function() {
        label = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE","JUL","AUG","SEP","OCT","NOV","DEC"];
        data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0];
        $( "#monthbtn" ).attr('disabled',true);
        $( "#monthbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    });

    $( "#yearbtn" ).click(function() {
        label = ["2016","2017","2018","2019"];
        data = [0,0,123,0];
        $( "#yearbtn" ).attr('disabled',true);
        $( "#yearbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    });

    

    function rendergraph(data,label)
    {
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    label: 'Total Books Sold',
                    data: data,
                    backgroundColor:'rgba(18,172,82,0.6)',
                    borderColor:'#02522A',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                tooltips: {
					mode: 'index',
					intersect: false,
				}
            }
        });
    }


</script>

@endsection
