<div class="modal fade" id="addInventoryModal" tabindex="-1" role="dialog" aria-labelledby="AddInventory" aria-hidden="true">
    {{ Form::open(['url' => 'store_invs', 'files' => 'true']) }}
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Add Inventory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
             {{ Form::label('select_book', 'Select Book') }} 
             <select  name='book_id' id='book_id' >
          
             </select>
            </div>
            <div class="form-group book_preview">

              <table class="table table-bordered table-striped small">
                <tr>
                    <td id='book_image' width="150px" rowspan="4"></td>
                    <td>Name</td>
                    <td id='book_name'></td>
                </tr>
                <tr>
                    <td>Author</td>
                    <td id='book_author' ></td>
                </tr>
                <tr>
                    <td>Detail</td>
                    <td id='book_detail'></td> 
                </tr>
              </table>
            </div>
            <div class="form-group">
                {{ Form::label('lbl_price', 'Price') }} {{ Form::text('price', '0.00', array('class' => 'form-control', 'required')) }}
            </div>
            <div class="form-group">
                {{ Form::label('lbl_discount', 'Discount') }} {{ Form::text('discount', '0.00', array('class' => 'form-control', 'required')) }}
            </div>

            
          

    <br />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{ Form::hidden('store_id', $store->id,array('id' => 'store_id')) }}
            {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}     
          </div>
        </div>
      </div>
    {{ Form::close() }}
    </div>


    @section('js')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<script>
       

  $(document).ready(function()
  {

 
      $('#book_id').select2({
            theme: "bootstrap",
            dropdownParent: $("#addInventoryModal"),
            ajax: {
                url: '/select2/getNotInStoreBook',
                dataType: 'json',
                delay: 200,
                data: function(params) {

                    var query = {
                        q: params.term,
                        page: params.page,
                        store_id: $('#store_id').val(),
                    };

                return query;
                },
                 processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }

              
            },
    
            templateResult: function(repo) {
                if (repo.loading) return repo.name;
                var markup = repo.name;
                return markup;
            },
            templateSelection: function(repo) {
                $.getJSON("{{ url('ajax/getBookByID')}}", 
                      { book_id: repo.id }, 
                      function(data) {
                       $('#book_name').html(data.book.name);
                       $('#book_detail').html(data.book.detail);
                       $('#book_author').html(data.book.author);
                       $('#book_image').html( '<img class="card-img-top" src="'+data.image+'">');
                          //alert(data.id);
                      });
                return repo.name;
            },
            escapeMarkup: function(markup) {
                return markup;
            }


        });
    // $('#book_id').change(function(){
    //     $.getJSON("{{ url('select/getNotInStoreBook')}}", 
    //         { store_id: $('#store_id').val() }, 
    //         function(data) {
    //             var model = $('#book_id');
    //             model.empty();

              
    //             $.each(data, function(index,element) {
    //                 model.append("<option value='"+element.id+"'>" + element.name + "</option>");
    //             });
    //         });
    // });
    //  $('#book_id').change();


   
  });
   
</script>
@endsection