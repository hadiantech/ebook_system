@extends('layouts.basic.master') 
@section('content')

<!-- Page Heading -->
<section class="welcome bg-green" style="background-image:url('https://picsum.photos/1080/400'); background-size:cover; background-repeat:no-repeat">
    <div class="container-fluid p-0 m-0">
        <div class="container">
            <div class="row title align-items-center">
                <div class="col-sm-12">
                    <h1 class="text-white text-shadow">Welcome to LeafBook</h1>
                    <p class="text-white text-shadow">Digital publishing platform</p>
                    <a href="" class="btn btn-orange">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="trial">
    <div class="container-fluid p-0 m-0">
        <div class="container p-5">
            <div class="row justify-content-center align-items-center text-center">
                <div class="col-sm-12 my-1"><img src="img/logo_sm.png" class="img-fluid" style="height: 60px;"></div>
                <div class="col-sm-12 my-2">
                    <h2 class="oswald thin m-0">LeafBook Author</h2>
                </div>
                <div class="col-sm-6">
                    <p>Create beautiful EPUB-based interactive ebooks that run across all devices and platforms</p>
                </div>
            </div>

            @foreach($packages as $package)
                @include('shared._package_view', ['package' => $package, 'view' => 'user'])
            @endforeach

            <div class="row my-3 justify-content-center align-items-center">
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="card text-white bg-success mb-3">
                        <div class="card-header">Premium</div>
                        <div class="card-body">
                            <h5 class="card-title">RM999</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="" class="btn btn-green">BUY</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="card text-white bg-secondary mb-3">
                        <div class="card-header">Basic</div>
                        <div class="card-body">
                            <h5 class="card-title">RM99</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="" class="btn btn-green"> BUY</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="card bg-light mb-3">
                        <div class="card-header">FREE</div>
                        <div class="card-body">
                            <h5 class="card-title">RM0</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="" class="btn btn-green"> BUY</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="signup bg-gray">
    <div class="container-fluid p-0 m-0">
        <div class="container p-5">
            <div class="row justify-content-center align-items-center text-center">
                <div class="col-sm-12">
                    <h1>Get Started With LeafBook</h1>
                </div>
                <div class="col-sm-12 row justify-content-center">
                    <div class="form-group form-inline">
                        <input class="form-control m-2" type="text" placeholder="Name" />
                        <input class="form-control m-2" type="text" placeholder="Email" />
                        <a href="" class="btn btn-green m-2">Get your free license</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
