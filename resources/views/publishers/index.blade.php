@extends('layouts.dashboard.master') 
@section('title', 'Publisher Management') 
@section('content')

<div class="col-sm-12 mb-2 p-0">
    <div class="mb-3">
        <div class="d-flex flex-row-reverse">
            @can('add_publishers')
                <a href="{{ route('publishers.create') }}" class="btn btn-success">
                    <i class="fas fa-plus mr-2"></i> Add New
                </a>
            @endcan
        </div>
    </div>
</div>

<div class="col-sm-12 card p-4">
    <table id="publishers-table" class="table table-bordered table-hovered">
        <thead class="thead-dark ">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Owner</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>


@endsection

@section('js') 
@parent

<script>
    $(function() {
        $('#publishers-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getPublisher',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name',
                    name: 'publishers.name'
                },
                {
                    data: 'owner.name',
                    name: 'owner.name'
                },
                {
                    data: 'created_at',
                    name: 'publishers.created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
