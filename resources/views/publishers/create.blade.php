@extends('layouts.dashboard.master') 
@section('title', 'Publisher Management') 
@section('subtitle', 'Create')
@section('content')


<div class="col-lg-12 card p-4">
    
    {{ Form::open(array('url' => 'publishers')) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('detail', 'Detail') }} {{ Form::textarea('detail', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('owner', 'Owner') }}
        <select id="owner" name="owner" class="select2 form-control"></select>
    </div>

    <br />
    <a class="btn btn-link" href="{{ route('publishers.index') }}">Back</a>
    
    {{ Form::button('<i class="fa fa-check mr-1"></i> Add', array('class' => 'btn btn-success', 'type' => 'submit')) }} 
    {{ Form::close() }}
</div>


@endsection @section('js') @parent
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap",
            ajax: {
                url: '/select2/getUser',
                dataType: 'json',
                delay: 200,
                data: function(params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }
            },
            minimumInputLength: 1,
            templateResult: function(repo) {
                if (repo.loading) return repo.name;
                var markup = repo.name + ', ' + repo.email;
                return markup;
            },
            templateSelection: function(repo) {
                return repo.name;
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });
    });

</script>
@endsection
