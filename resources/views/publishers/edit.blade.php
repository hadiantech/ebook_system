@extends('layouts.dashboard.master') 
@section('title', 'Publisher Management') 
@section('subtitle', 'Edit') 
@section('content')

<div class="col-sm-12 bg-white border p-4">
    {{ Form::model($publisher, ['route' => ['publishers.update', $publisher->id], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('detail', 'Detail') }} {{ Form::textarea('detail', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('owner', 'Owner') }} {{ Form::text('owner[name]', null, array('class' => 'form-control', 'disabled')) }}
    </div>
    <br />
    <a class="btn btn-link" href="{{ route('publishers.index') }}">Back</a> 
    {{ Form::button('Save', array('class' => 'btn btn-success', 'type' => 'submit')) }} 
    {{ Form::close() }}
</div>
@endsection
