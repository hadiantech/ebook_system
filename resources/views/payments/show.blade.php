@extends('layouts.dashboard.master')
@section('title', 'PAYMENT CENTER')
@section('subtitle', '')
@section('content')

<div class="col-md-12 mb-4">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">1802-5</h2>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="oswald thin">leafbook</h2>
                        <p> 
                            Address 1, <br/>
                            Address 2, <br/>
                            City, Postcode <br/>
                            State
                        </p>
                        <p class="bold m-0">Bill to:</p>
                        <p>
                            User Name <br/>
                            Email <br/>
                        </p>
                    </div>
                    <div class="col-md-6 text-right">
                        <p>Date: 30 July 2018</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr />
                    </div>
                    <div class="col-md-12">
                        <table class="table table-borderless table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Description</th>
                                    <th scope="col">Amount (RM)</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="80% ">Leafbook GOLD Package</td>
                                    <td>99.99</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-borderless table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td width="80%" class="text-right bold">Tax</td>
                                    <td>0.00</td>
                                </tr>
                                <tr>
                                    <td width="80%" class="text-right bold">Total</td>
                                    <td class="bold">99.99</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row d-print-none">
                    <div class="col-md-12">
                        <a href="javascript:window.print()" class="btn btn-info">Print Invoice</a>
                        <a href="{{ route('payments.selectpayment') }}" class="btn btn-success">Make Payment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 
@section('js') 
@parent
{{-- js here --}}
@endsection