@extends('layouts.dashboard.master')
@section('title', 'PAYMENT CENTER')
@section('subtitle', '')
@section('content')

<div class="col-md-12 mb-4">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('payments.selectpayment') }}" class="btn btn-success mb-4 mx-0"><i class="fa fa-chevron-left mr-2"></i> Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">1802-5</h2>
                <p>30 July 2018</p>
            </div>
            <div class="col-md-12 mb-3">
                <div class="form-group border rounded p-3 border-success">
                    <h5 class="oswald thin mb-4">1. Perform online transfer to:</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td width="30%" class="text-right">Bank Name</td>
                                        <td class="bold">CIMB</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="text-right">Account Name</td>
                                        <td class="bold">Leafbook Sdn Bhd</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" class="text-right">Account Number</td>
                                        <td class="bold">871623720</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 mt-4">
                            <p>Payment details</p>
                            <p class="m-0">Invoice number: <span class="bold">1802-5</span></p>
                            <p>Date: <span class="bold">30 July 2018</span></p>
                            <table class="table table-borderless table-striped table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Description</th>
                                        <th scope="col">Amount (RM)</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="80% ">Leafbook GOLD Package</td>
                                        <td>99.99</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-borderless table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td width="80%" class="text-right bold">Tax</td>
                                        <td>0.00</td>
                                    </tr>
                                    <tr>
                                        <td width="80%" class="text-right bold">Total Amount</td>
                                        <td class="bold">99.99</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="form-group border rounded p-3 border-success">
                    <h5 class="oswald thin mb-4">2. Upload proof of transaction</h5>
                    <input type="file" class="form-control col-md-6" />
                </div>
                <div class="form-group">
                    <a href="" class="btn btn-success" style="width:120px" data-toggle="modal" data-target="#exampleModal">Submit</a>
                </div>
            </div>
        </div>
        <div class="row border m-1">
            <p class="m-4">---Admin View Only---</p>
            <div class="col-md-12 border rounded m-3 p-4">
                <a href="" class="">Download document (proof of transaction)</a>
            </div>
            <div class="col-md-12 border rounded m-3 p-4">
                <p class="">Current Status <span class="badge badge-success p-2">PAID</span></p>
                <p class="">Current Status <span class="badge badge-warning p-2">UNPAID</span></p>
                <a href="" class="btn btn-success">CHANGE STATUS TO PAID</a>
                <a href="" class="btn btn-warning">CHANGE STATUS TO UNPAID</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content p-5">
      <div class="modal-body p-0">
        <h1 class="oswald thin mb-3">Thank You!</h1>
        <p>Our ninjas will review the payment document, and it will be reflected in the system within working 2-5 days.</p>
      </div>
      <div class="modal-footer p-0">
        <button type="button" class="btn btn-success btn-block m-0" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

@endsection 
@section('js') 
@parent
{{-- js here --}}
@endsection