@extends('layouts.dashboard.master')
@section('title', 'PAYMENT CENTER')
@section('subtitle', '')
@section('content')

<div class="col-md-12 mb-4">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">Invoices</h2>
                <p>List of available invoices</p>
            </div>
            <div class="col-md-12">
                <table class="table table-borderless table-striped table-bordered">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Invoice #</th>
                        <th scope="col">Issued To</th> 
                        <th scope="col">Issued Date</th>
                        <th scope="col">Total (RM)</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row">1802-5</th>
                        <td>User Name</td>
                        <td>30 July 2018</td>
                        <td>99.99</td>
                        <td><span class="badge badge-warning p-2" style="width:60px">Unpaid</span></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary" style="padding:5px 10px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('payments.show') }}">View</a>
                                    <a class="dropdown-item" href="{{ route('payments.selectpayment') }}">Pay</a>
                                    <a class="dropdown-item text-red" data-method="delete" data-confirm="confirm" href="">Delete</a>
                                </div>
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">1802-3</th>
                        <td>User Name</td>
                        <td>30 June 2018</td>
                        <td>99.99</td>
                        <td><span class="badge badge-success p-2" style="width:60px">Paid</span></td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary" style="padding:5px 10px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('payments.show') }}">View</a>
                                    <a class="dropdown-item" href="{{ route('payments.selectpayment') }}">Pay</a>
                                    <a class="dropdown-item text-red" data-method="delete" data-confirm="confirm" href="">Delete</a>
                                </div>
                            </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>

@endsection 
@section('js') 
@parent
{{-- js here --}}
@endsection