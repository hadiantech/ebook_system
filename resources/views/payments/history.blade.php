@extends('layouts.dashboard.master')
@section('title', 'PAYMENT HISTORY')
@section('subtitle', '')
@section('content')
<div class="col-md-12 mb-4 d-print-none">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-borderless table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Invoice #</th>
                            <th scope="col">Date</th> 
                            <th scope="col">Receipt</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1801-32</td>
                            <td>30 July 2018</td>
                            <td><a href="" class="" style="width:120px" data-toggle="modal" data-target="#exampleModal">1872389</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg printed">
        <div class="modal-content p-5">
            <div class="modal-body p-0">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <h2 class="">RECEIPT</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="oswald thin">leafbook</h2>
                                <p> 
                                    Address 1, <br/>
                                    Address 2, <br/>
                                    City, Postcode <br/>
                                    State
                                </p>
                                <p class="bold m-0">Bill to:</p>
                                <p>
                                    User Name <br/>
                                    Email <br/>
                                </p>
                            </div>
                            <div class="col-md-6 text-right">
                                <p class="m-0">Date: 30 July 2018</p>
                                <p>Inv no: 31802-5</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr />
                            </div>
                            <div class="col-md-12">
                                <table class="table table-borderless table-striped table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Description</th>
                                            <th scope="col">Amount (RM)</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td width="80% ">Leafbook GOLD Package</td>
                                            <td>99.99</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-borderless table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <td width="80%" class="text-right bold">Tax</td>
                                            <td>0.00</td>
                                        </tr>
                                        <tr>
                                            <td width="80%" class="text-right bold">Total</td>
                                            <td class="bold">99.99</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row d-print-none">
                            <div class="col-md-12">
                                <a href="javascript:window.print()" class="btn btn-info">Print</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer p-0 d-print-none">
                <button type="button" class="btn btn-success btn-block m-0" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('js') 
@parent
{{-- js here --}}
@endsection