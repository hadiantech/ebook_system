@extends('layouts.dashboard.master')
@section('title', 'PAYMENT CENTER')
@section('subtitle', '')
@section('content')

<div class="col-md-12 mb-4">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('payments') }}" class="btn btn-success mb-4 ml-0"><i class="fa fa-chevron-left mr-2"></i> Back</a>
            </div>
        </div>
        <div class="row align-items-stretch">
            <div class="col-md-6 text-center">
                <div class="border border-success rounded p-5 d-flex flex-column justify-content-between" style="height:100%">
                    <i class="fas fa-money-check-alt text-green mb-5" style="font-size:4em;"></i>
                    <h1 class="oswald thin">Online Banking</h1>
                    <p>Credit card and Debit card paymnent</p>
                    <a href="" class="btn btn-green btn-block m-0 disabled">COMING SOON</a>
                </div>
            </div>
            <div class="col-md-6 text-center">
                <div class="border border-success rounded p-5 d-flex flex-column justify-content-between" style="height:100%">
                    <i class="fas fa-university text-green mb-5" style="font-size:4em;"></i>
                    <h1 class="oswald thin">Online Transfer</h1>
                    <p>Transfer the amount to our account, then upload the receipt/proof here.</p>
                    <a href="{{ route('payments.edit') }}" class="btn btn-green btn-block m-0">Select</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 
@section('js') 
@parent
{{-- js here --}}
@endsection