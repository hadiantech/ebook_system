<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-lg-12 p-0">
            <!-- Page Heading -->
            <h1 class="my-4">@yield('title')
                <small>@yield('subtitle')</small>
            </h1>
            @include('flash::message')
            @yield('content')
        </div>
    </div>
</div>
