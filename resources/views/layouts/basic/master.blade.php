<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0-beta.3/journal/bootstrap.min.css" rel="stylesheet" integrity="sha384-/qQob+A1P2FeRYphuSbqp7heEKdhjZxqgAz/yWLX9CQKU9FTB8wKTexI0IiFlIXC" crossorigin="anonymous">
      
    <link href="{!! asset('css/basic_bootstrap.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/main.css') !!}" media="all" rel="stylesheet" type="text/css" />

    @yield('css')

  </head>

  <body>

    @include('cookieConsent::index')

    @include('layouts.basic.navbar')
    <!-- Page Content -->
    @include('layouts.basic.content')
    <!-- /.container -->
    @include('layouts.basic.footer')


    @include('shared._delete_confirm')

    <script src="{{ mix('/js/app.js') }}"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    @yield('js')

  </body>

</html>
