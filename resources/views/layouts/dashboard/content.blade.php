<div class="d-flex">
    <nav class="d-none d-md-block sidebar d-print-none" style="margin-top:60px;">
        <div class="sidebar-sticky">
            <ul class="nav flex-column">

                {{-- PUBLISHER MENU --}}
                <li class="nav-item">
                    <a class="nav-link">PUBLISHER MENU</a>
                </li>

                <li class="nav-item {{ Request::is('analytics') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('analytics')}}"><i class="far fa-chart-bar"></i> ANALYTICS & REPORTS</a>
                </li>

                <li class="nav-item {{ Request::is('books') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('books.index')}}"><i class="fas fa-book"></i> BOOK CATALOGUE</a>
                </li>

                <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                    <a href="javascript:;" class="nav-link submenu" href=""><i class="fas fa-window-restore"></i> BOOK STORE <i class="ml-2 fa fa-chevron-down"></i></a>
                    <ul class="sub-nav">
                        <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                            <a class="nav-link submenu" href="{{ route('stores.create','') }}"><i class="fas fa-plus"></i> SETUP A NEW STORE</a>
                        </li>
                        <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                                <a class="nav-link submenu" href="{{ route('stores.index') }}"><i class="fas fa-plus"></i> VIEW STORE</a>
                            </li>
                        <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                            <a class="nav-link submenu" href="{{ route('payments') }}"><i class="fas fa-stream"></i> INVENTORY</a>
                        </li>
                        <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                            <a class="nav-link submenu" href="{{ route('payments') }}"><i class="fas fa-file-invoice-dollar"></i> SALES</a>
                        </li>
                        <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('payments.history')}}"><i class="fas fa-cog"></i> SETTING</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::is('payments') ? 'active' : '' }}">
                    <a href="javascript:;" class="nav-link submenu" href=""><i class="fa fa-credit-card"></i> PAYMENT CENTER <i class="ml-2 fa fa-chevron-down"></i></a>
                    <ul class="sub-nav">
                        <li class="nav-item {{ Request::is('payments') ? 'active' : '' }}">
                            <a class="nav-link submenu" href="{{ route('payments') }}"><i class="fas fa-file-invoice"></i> INVOICES</a>
                        </li>
                        <li class="nav-item {{ Request::is('payments') ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('payments.history')}}"><i class="fas fa-history"></i> HISTORY</a>
                        </li>
                    </ul>
                </li>

                {{-- ADMIN MENU --}}
                <li class="nav-item">
                    <a class="nav-link">ADMIN MENU</a>
                </li>

                <li class="nav-item {{ Request::is('publishers') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('publishers.index')}}"><i class="fas fa-users"></i> Publisher Management</a>
                </li>

                <li class="nav-item {{ Request::is('books') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('books.index')}}"><i class="fas fa-book"></i> Ebook Management</a>
                </li>

                <li class="nav-item {{ Request::is('stores') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('stores.index')}}"><i class="fas fa-window-restore"></i> Store Management</a>
                </li>

                <li class="nav-item {{ Request::is('creator') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('creator.index')}}"><i class="fab fa-creative-commons-share"></i> Ebook Creator</a>
                </li>

                <li class="nav-item {{ Request::is('packages') ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('packages.index')}}"><i class="fas fa-cubes"></i> Package Management</a>
                </li>
                

                <li class="nav-item">
                    <a href="javascript:;" class="nav-link submenu" href=""><i class="fa fa-cog"></i> Setting <i class="ml-2 fa fa-chevron-down"></i></a>
                    <ul class="sub-nav">
                        <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('users.index')}}"><i class="fas fa-user"></i> User Management</a>
                        </li>
                        <li class="nav-item {{ Request::is('tags') ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('tags.index')}}"><i class="fas fa-tags"></i> Tag Management</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('roles.index')}}"><i class="fas fa-users-cog"></i> Roles & Permission</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('profile')}}"><i class="fas fa-user"></i> Profile</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- REMINDER: PUT CONTENT INSIDE THIS DIV "content"  -->
    <div class="content">
        <div class="container-fluid p-0 d-print-none" style="margin-top:60px">
            <div class="card p-3 d-flex flex-row justify-content-between align-items-center" style="border:none; border-radius:0px">
                <h3 class="m-0 oswald thin">@yield('title')</h3>
                <p class="m-0">Home</p>
                {{-- <p class="m-0">Home @yield('title') @yield('subtitle')</p> --}}
            </div>
        </div>
        <div class="container p-4">
            <div class="row">
                @include('flash::message')
                @include('shared/_error')
                @yield('content')
            </div>
        </div>
    </div>
</div>
@section('js')
@parent
<script>
    var dropdown = document.getElementsByClassName("submenu");
    var i;

    for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
        } else {
        dropdownContent.style.display = "block";
        }
    });
    }
</script>
@endsection



