@extends('layouts.dashboard.master')
@section('title', 'ANALYTICS & REPORTS')
@section('subtitle', '')
@section('content')

{{-- BOOKS --}}

<div class="col-md-12 mb-4">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">TOP 3 DOWNLOADS</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($books->take(3) as $b)
            <div class="col-md-4">
                <div class="bg-white rounded">
                    <div class="bg-cover" style="min-height:280px; width:100%; background-image:url({{ $b->getMedia('cover')->first()->getUrl() }})"></div>
                    <p class="mt-3 mb-1">{{ $b->name }}</p>
                    <p class="mb-0" style="font-size:13px">By {{ $b->author }}</p>
                    <p class="mb-3" style="font-size:13px">Total Downloads: 10</p>
                    <a class="mb-3" style="font-size:13px" href="{{ route('books.show',$b->id) }}">View</a>
                </div>
            </div> 
            @endforeach
        </div>
        <div class="row flex-row-reverse mt-3">
            <div class="col-md-12 d-flex flex-row-reverse">
                <a href="{{ route('books.index') }}" class="btn btn-success btn-w-m">See All </a>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="bg-white p-4">
        <h2>{{ $books->count() }}</h2>
        <p>Total Books</p>
    </div>
</div>
<div class="col-md-3">
    <div class="bg-white p-4">
        <h2>0</h2>
        <p>Published</p>
    </div>
</div>
<div class="col-md-3">
    <div class="bg-white p-4">
        <h2>0</h2>
        <p>Not published</p>
    </div>
</div>
<div class="col-md-3">
    <div class="bg-white p-4">
        <h2>0</h2>
        <p>Draft</p>
    </div>
</div>

<div class="col-md-12 mt-4">
    <div class="bg-white p-4">
        <h2>BOOKS SALES</h2>
        <canvas id="myChart" style="width:100%; height:300px;"></canvas>
        <div class="p-3">
            <button id="weekbtn" class="btn btn-orange">Week</button>
            <button id="monthbtn" class="btn btn-orange" disabled>Month</button>
            <button  id="yearbtn" class="btn btn-orange">Year</button>
        </div>
    </div>
</div>

{{-- STORES --}}
<div class="col-md-12 mt-3">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">STORES</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($stores->take(1) as $s)
            <div class="col-md-4 p-4 d-flex flex-column justify-content-between">
                <div class="rounded border p-3">
                    <div class="d-flex">
                        <div class="d-flex flex-column">
                            <i class="fas fa-store-alt mb-2" style="font-size:2.5em"></i>
                            <span class="badge badge-pill badge-success">{{ $s->status }}</span>
                        </div>
                        <div class="ml-3">
                            <p class="bold newscycle" style="font-size:1.3em;">{{ $s->store_name }}</p>
                            <p class="m-0" style="font-size:13px">Sales: RM 4,423.00</p>
                            <p class="m-0" style="font-size:13px">Books: 42</p>
                            <p class="m-0" style="font-size:13px">Publisher: A</p>
                            
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <a href="{{ route('stores.index') }}" class="btn btn-outline-info btn-sm" style="padding:4px 15px">View Detail</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 p-4 d-flex flex-column justify-content-between">
                <div class="rounded border p-3">
                    <p class="bold newscycle" style="font-size:1.3em;">POPULAR CATEGORY</p>
                    @foreach ($tags as $t )
                    <span class="badge badge-pill badge-info">{{ $t->name }}</span>
                    @endforeach
                    <span class="badge badge-pill badge-info">. . .</span>

                    <div class="d-flex flex-row-reverse mt-3">
                        <a href="" class="btn btn-outline-info btn-sm" style="padding:4px 15px">More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 p-4 d-flex flex-column justify-content-between">
                <div class="rounded border p-3">
                    <p class="bold newscycle" style="font-size:1.3em;">TOP KEY SEARCH</p>
                    <span class="badge badge-pill badge-warning">naruto</span>
                    <span class="badge badge-pill badge-warning">anime</span>
                    <span class="badge badge-pill badge-warning">islam</span>
                    <span class="badge badge-pill badge-warning">najib</span>
                    
                
                    <div class="d-flex flex-row-reverse mt-3">
                        <a href="" class="btn btn-outline-info btn-sm" style="padding:4px 15px">More</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row flex-row-reverse mt-3">
            <div class="col-md-12 d-flex flex-row-reverse">
                <a href="{{ route('stores.index') }}" class="btn btn-success btn-w-m">See All </a>
            </div>
        </div>
    </div>
</div>

{{-- SUBSCRIPTION/PACKAGE --}}
<div class="col-md-12 mt-3">
    <div class="card p-4">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h2 class="">PACKAGE</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ( $packages as $p )
                <div class="row border border-warning rounded m-0 mb-3" style="border-width:2px !important">
                    <div class="col-md-12 d-flex p-3">
                        <div class="bg-contain p-2" style="background-image:url('{{ $p->getMedia('image')->first()->getUrl() }}'); width:150px; height:150px"></div>
                        <div class="mx-4">
                            <p class="m-0 newscycle mb-2" style="font-size:1.3em">{{ $p->title }}</p>
                            <p class="m-0">Start: 12/12/2018</p>
                            <p class="m-0">End: -</p>
                            <p class="m-0">Payment type: <span class="badge badge-pill badge-info">Recurring</span> <span style="font-size:12px">(charged 15th on every month)</span></p>
                            <div>
                                <a href="" class="btn btn-outline-info btn-sm px-2 py-1 m-0 mt-2">Payment History</a>
                                <a href="" class="btn btn-outline-info btn-sm px-2 py-1 m-0 mt-2">Manage Package</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endforeach

                {{-- PACKAGE FEATURE LIST --}}
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header p-0" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link text-green" style="text-decoration:none" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                FEATURES <i class="ml-3 fa fa-chevron-down"></i>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 p-3">
                                        <p>Storage (33MB/100MB)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:33%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-3">
                                        <p>Books Uploaded (3/100)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:3%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-3">
                                        <p>Books Created (15/100)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:15%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-3">
                                        <p>Store Inventory (18/100)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:18%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-3">
                                        <p>Store (1/1)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:100%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-3">
                                        <p>Promotion (0/100)</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width:0%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 p-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck" checked>
                                            <label class="custom-control-label" for="customCheck">Max size book upload (per book): 20MB</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                            <label class="custom-control-label" for="customCheck1">Add new publisher</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck2" checked>
                                            <label class="custom-control-label" for="customCheck2">Add new user</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck3" checked>
                                            <label class="custom-control-label" for="customCheck3">Analytics access</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection 
@section('js') 
@parent

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>

    label = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE","JUL","AUG","SEP","OCT","NOV","DEC"];
    data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0];
    rendergraph(data,label)

    $( "#weekbtn" ).click(function() {
        label = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"];
        data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0,12, 19, 3, 5, 2, 3,0,0,0,0,0,0,4,5,6,7,8,9];
        $( "#weekbtn" ).attr('disabled',true);
        $( "#weekbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    
    });

    $( "#monthbtn" ).click(function() {
        label = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE","JUL","AUG","SEP","OCT","NOV","DEC"];
        data = [12, 19, 3, 5, 2, 3,0,0,0,0,0,0];
        $( "#monthbtn" ).attr('disabled',true);
        $( "#monthbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    });

    $( "#yearbtn" ).click(function() {
        label = ["2016","2017","2018","2019"];
        data = [0,0,123,0];
        $( "#yearbtn" ).attr('disabled',true);
        $( "#yearbtn" ).siblings().attr('disabled',false);
        rendergraph(data,label);
    });

    

    function rendergraph(data,label)
    {
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: label,
                datasets: [{
                    label: 'Total Books Sold',
                    data: data,
                    backgroundColor: [
                        'rgba(18,172,82,0.6)'
                    ],
                    borderColor: [
                        '#02522A'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    }


</script>
@endsection