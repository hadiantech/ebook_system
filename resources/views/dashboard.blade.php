@extends('layouts.dashboard.master')
@section('title', 'Dashboard')
@section('content')

<div class="container-fluid">
    <div class="container">
        <h1>DASHBOARD</h1>
        <div class="row">
            <div class="col-sm-12">
                <div class="card mb-3">
                    <img class="card-img-top" src="https://picsum.photos/1000/280" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Statistic</h5>
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <div class="card bg-dark text-white">
                    <img class="card-img" src="https://picsum.photos/300" alt="Card image">
                    <div class="card-img-overlay">
                        <h1 class="card-title">28</h1>
                        <p class="card-text">Publishers</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card bg-dark text-white">
                    <img class="card-img" src="https://picsum.photos/300" alt="Card image">
                    <div class="card-img-overlay">
                        <h1 class="card-title">2</h1>
                        <p class="card-text">Publishers</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card bg-dark text-white">
                    <img class="card-img" src="https://picsum.photos/300" alt="Card image">
                    <div class="card-img-overlay">
                        <h1 class="card-title">5</h1>
                        <p class="card-text">Publishers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
