@extends('layouts.dashboard.master') 
@section('title', 'Tag Management')
@section('subtitle', 'All Tags')
@section('content')

<div class="col-sm-12 mb-4 p-0">
    <div class="">
        <div class="d-flex flex-row-reverse">
            @can('add_tags')
                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#roleModal">
                    <i class="fas fa-plus"></i> New
                </a> @endcan
        </div>
    </div>
</div>

<div class="col-sm-12 card p-4">
    <table id="tags-table" class="table table-bordered table-hover">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel">
    <div class="modal-dialog" role="document">
        {!! Form::open(['method' => 'post']) !!}

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tags</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <!-- name Form Input -->
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    {!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Tag Name', 'required']) !!} @if ($errors->has('name'))
                    <p class="help-block">{{ $errors->first('name') }}</p> @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                <!-- Submit Form Button -->
                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection @section('js') @parent

<script>
    $(function() {
        $('#tags-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getTag',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name'
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
