@extends('layouts.dashboard.master') 
@section('title', 'Tag Management') 
@section('subtitle', 'Edit') 
@section('content')


<div class="col-lg-12 card p-4">

    {{ Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <a class="btn btn-link" href="{{ route('tags.index') }}">Back</a> 
    {{ Form::button('Save', array('class' => 'btn btn-success', 'type' => 'submit')) }} 
    {{ Form::close() }}

</div>


@endsection
