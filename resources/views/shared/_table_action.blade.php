<div class="dropdown">
  <button class="btn btn-secondary" style="padding:5px 10px" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-chevron-down"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      @can('view_'.$url)
      <a class="dropdown-item" href="{{route($url.'.show', $model->id)}}">View</a>
      @endcan
    
      @can('edit_'.$url)
      <a class="dropdown-item" href="{{route($url.'.edit', $model->id)}}">Edit</a>
      @endcan
    
      @can('delete_'.$url)
      <a class="dropdown-item text-red" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
      @endcan
  </div>
</div>


