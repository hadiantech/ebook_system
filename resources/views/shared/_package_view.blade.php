<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="{{ $package->getMedia('image')->first()->getUrl()}} " alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title">{{ $package->title }}</h5>
        <p class="card-text">
            @if($package->started_at != null)
            <small class="text-muted">Started at: {{ $package->started_at }}</small>
            <br />
            @endif

            @if($package->ended_at != null)
            <small class="text-muted">Ended at: {{ $package->ended_at }}</small>
            <br />
            @endif

            {{ $package->description }}
            <br />
            Price : RM {{ $package->price }}

            <ul class="list-group">
                <li class="list-group-item">Books Upload : {{ $package->books_upload or 'Unlimited' }}</li>
                <li class="list-group-item">Books Created : {{ $package->books_created or 'Unlimited' }}</li>
                <li class="list-group-item">Store : {{ $package->store or 'Unlimited' }}</li>
                <li class="list-group-item">Books In Store : {{ $package->store_inv or 'Unlimited' }}</li>
                <li class="list-group-item">Promotion : {{ $package->promotion or 'Unlimited' }}</li>
            </ul>
            
        </p>

        @isset($view)
            @switch($view)
                @case('admin')
                <a href="{{ route('packages.show', $package->id) }}" class="btn btn-success btn-block">Show</a>
                <a href="{{ route('packages.edit', $package->id) }}" class="btn btn-primary btn-block">Edit</a>
                @break

                @case('user')
                <a href="{{ route('packages.show', $package->id) }}" class="btn btn-success btn-block">Buy</a>
                @break

            @endswitch
        @endisset

    </div>
</div>