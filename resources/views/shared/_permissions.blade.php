<div class="card">
    <div class="card-header" id="{{ str_slug($title) }}">
        <h5 class="mb-0">
            <button type="button" class="btn btn-link float-left" data-toggle="collapse" data-target="#dd-{{ str_slug($title) }}">
                {{ $title or 'Override Permissions' }}
            </button>
        </h5>
    </div>

    <div id="dd-{{ str_slug($title) }}" class="collapse  {{ $closed or 'show' }}"  data-parent="#accordion">
        <div class="card-body">
            <div class="row">
                @foreach($permissions as $perm)
                    <?php
                    $per_found = null;

                    if( isset($role) ) {
                        $per_found = $role->hasPermissionTo($perm->name);
                    }

                    if( isset($user)) {
                        $per_found = $user->hasDirectPermission($perm->name);
                    }
                    ?>

                    <div class="col-md-3">
                        <div class="checkbox">
                            <label class="{{ str_contains($perm->name, 'delete') ? 'text-danger' : '' }}">
                                {!! Form::checkbox("permissions[]", $perm->name, $per_found, isset($options) ? $options : []) !!} {{ $perm->name }}
                            </label>
                        </div>
                    </div>
                @endforeach

                    @if($btn_hide != true)
                            <div class="col-md-12">
                                @can('edit_roles')
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                @endcan

                                <a class="btn btn-danger" data-method="delete" data-confirm="confirm" href="{{ route('roles.destroy', $role->id) }}">Delete</a>
                            </div>
                    @endif
            </div>
        </div>
    </div>
</div>

