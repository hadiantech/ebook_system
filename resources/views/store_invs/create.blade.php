@extends('layouts.dashboard.master') @section('title', 'Store Management') @section('subtitle', 'Create') @section('content')

<div class="col-sm-12 bg-white border p-4">
    {{ Form::open(['url' => 'stores', 'files' => 'true']) }}

    <div class="form-group">
        {{ Form::label('name', 'Store Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('type_id', '') }}
        <select id="type" name="type" class="select2 form-control" required>
             <option value="General" selected='selected'>General</option>
             <option value="Private">Private</option>
        </select>
    </div>

    <div class="form-group" id="publisher_hid" >
        {{ Form::label('publisher_id', 'Publisher') }}
        <select id="publisher_id" name="publisher_id" class="form-control" required></select>
    </div>

     <div class="form-group">
        {{ Form::label('status', 'Status') }}
        <select id="status" name="status" class="form-control" required>
            <option value="Active" selected='selected'>Active</option>
             <option value="Closed">Closed</option>
        </select>
    </div>

    <br />
    <a class="btn btn-link" href="{{ route('books.index') }}">Back</a> {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} {{ Form::close() }}
</div>
@endsection @section('js') @parent
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>



<script>
    $(document).ready(function() {

        $('#type').change(function () {
        var str = this.value;
        if (str=='General')
                $('#publisher_hid').hide();
            else $('#publisher_hid').show();
        }).change();
        

        $('#publisher_id').select2({
            theme: "bootstrap",
            ajax: {
                url: '/select2/getBook',
                dataType: 'json',
                delay: 200,
                data: function(params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                        pagination: {
                            more: (params.page * 10) < data.total
                        }
                    };
                }
            },
            minimumInputLength: 1,
            templateResult: function(repo) {
                if (repo.loading) return repo.name;
                var markup = repo.name;
                return markup;
            },
            templateSelection: function(repo) {
                return repo.name;
            },
            escapeMarkup: function(markup) {
                return markup;
            }
        });

    });

    $('#inputGroupFile01').on('change', function() {
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#inputGroupFile02').on('change', function() {
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })

</script>
@endsection
