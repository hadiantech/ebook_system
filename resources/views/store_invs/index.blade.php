@extends('layouts.dashboard.master') @section('title', 'Store Management') @section('content')

<div class="col-sm-12 mb-4">
    <div class="card">
        <div class="card-body">
            @can('add_stores')
            <a href="{{ route('stores.create') }}" class="btn btn-success">
                    <i class="fas fa-plus"></i> New
                </a> @endcan
        </div>
    </div>

</div>

<div class="col-sm-12">
    <table id="stores-table" class="table table-bordered table-hover">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@endsection @section('js') @parent

<script>
    $(function() {
        $('#stores-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '/datatables/getStore',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization');
                }
            },
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'store_name',
                    name: 'stores.name'
                },
                {
                    data: 'type',
                    name: 'stores.type'
                },
                {
                    data: 'status',
                    name: 'stores.status'
                },
                {
                    data: 'created_at',
                    name: 'stores.created_at'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });
    });

</script>

@endsection
