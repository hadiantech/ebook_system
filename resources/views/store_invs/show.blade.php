@extends('layouts.dashboard.master') @section('title', 'Store Management') @section('subtitle', '') @section('content')

<div class='col-lg-12 bg-white border p-3 '>

    {{ Form::model($store_invs, ['route' => ['storeInvs.update', $store_invs->id], 'method' => 'PUT']) }}
      <div >
        <div >
          <div>
            <h5>Update Inventory</h5>
          </div>
          <div >
            <div class="form-group book_preview">

              <table class="table table-bordered table-striped small">
                <tr>
            <td width="300px" rowspan="4">
                    <div class="card" style="width: 18rem;">
                        @if(!$store_invs->item_info->getMedia('cover')->first())
                        <img class="card-img-top" src="http://via.placeholder.com/300x300">
                        @else

                        <img class="card-img-top" src="{{ $store_invs->item_info->getMedia('cover')->first()->getUrl()}} ">
                        @endif
                    </div>
            </td>

            <td>Name</td>
            <td>{{$store_invs->item_info->name}}</td>
        </tr>
        {{-- disable if dont want allow username as auth --}}
        <tr>
            <td>Author</td>
            <td>{{$store_invs->item_info->author}}</td>
        </tr>
        <tr>
            <td>Epub</td>
            <td><a href="{{ $store_invs->item_info->getMedia('epub')->first()->getUrl()}}">Download</a></td>
            @javascript('epub_url', $store_invs->item_info->getMedia('epub')->first()->getUrl())
        </tr>
        <tr>
            <td>Tags</td>
            <td>
                @foreach($store_invs->item_info->tags as $tag)
                    {{ $tag->name }} @if (!$loop->last) ,@endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td></td>
            <td>Detail</td>
            <td>{{$store_invs->item_info->detail}}</td> 
        </tr>
              </table>
            </div>
            <div class="form-group">
                {{ Form::label('lbl_price', 'Price') }} {{ Form::number('price',$store_invs->price, array('class' => 'form-control', 'required' ,'id' => 'price','step'=>'.01')) }}
            </div>
            <div class="form-group">
                {{ Form::label('lbl_discount', 'Discount (%)') }} {{ Form::number('discount', $store_invs->discount, array('class' => 'form-control', 'required' ,'id' => 'discount','step'=>'.01' )) }}
            </div>
             <div class="form-group">
                {{ Form::label('lbl_total', 'Total Price') }} {{ Form::text('total', '0.00', array('class' => 'form-control', 'readonly' ,'id' => 'total')) }}
            </div>
    <br />
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="window.location.href='/stores/{{$store_invs->store_id}}'" >Back To Store</button>
            {{ Form::hidden('store_id', $store_invs->store_id,array('id' => 'store_id')) }}
            {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}     
          </div>
        </div>
      </div>
    {{ Form::close() }}

            
          

</div>

@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/epub.js/0.2.15/epub.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $(document).ready(function()
  {

     calculateTotal();
     $('#price').keyup(function(){
       calculateTotal();
     });

    $('#discount').keyup(function(){
       calculateTotal();
     });

  });

    function calculateTotal(){
            price= $('#price').val();
            discount =$('#discount').val();
            total =(price-(price*(discount/100))).toFixed(2);
            $('#total').val( total);

    }
   
   
</script>
@endsection
