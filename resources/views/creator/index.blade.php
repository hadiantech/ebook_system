@extends('layouts.dashboard.master') 
@section('title', 'Ebook Creator')
@section('subtitle', 'All Epubs')

@section('content')

<div class="col-sm-12 mb-2">
    <div class="">
        <div class="d-flex flex-row-reverse">
            <a class="btn btn-success" href="{{ route('creator.create') }}"><i class="fa fa-plus mr-2"></i> Create New Ebook</a>
        </div>
    </div>
</div>

<div class="col-sm-12">
    List of Draft
    <table id="publishers-table" class="table table-bordered table-hover">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
            @foreach($drafts as $draft)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $draft->title }}</td>
                <td>{{ $draft->created_at }}</td>
                <td>
                    <a class="btn btn-sm btn-success" href="{{ route('creator.show', $draft->id) }}">View</a>
                </td>
            </tr>
            @endforeach
        </thead>
    </table>
</div>

<div class="col-sm-12">
    List of Book Saved
    <table id="publishers-table" class="table table-bordered table-hovered">
        <thead class="thead-dark ">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<div class="col-sm-12">
    List of Preview Book
    <table id="publishers-table" class="table table-bordered table-hovered">
        <thead class="thead-dark ">
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Created At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

@endsection 
