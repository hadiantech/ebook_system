@extends('layouts.dashboard.master') 
@section('title', 'Book Management') 
@section('subtitle', '') 
@section('content')

<div class='col-lg-12 bg-white border p-3 '>
    <table class="table table-bordered table-striped">
        <tr>
            <td width="300px" rowspan="15">
                    <div class="card" style="width: 18rem;">
                        @if(!$draft->getMedia('cover')->first())
                        <img class="card-img-top" src="http://via.placeholder.com/300x300">
                        @else

                        <img class="card-img-top" src="{{ $draft->getMedia('cover')->first()->getUrl()}} ">
                        @endif
                    </div>
                    <div class="card-body">
                        @include('shared._upload_image', ['collection' => 'cover', 'model_name' => 'EpubDraft', 'model_id' => $draft->id])

                        <hr />
                        <a class="btn btn-success btn-block" href="{{ route('creator.publish', $draft->id) }}">Publish Book</a>
                        <a class="btn btn-primary btn-block" href="{{ route('creator.chapter.create', $draft->id) }}">Add Chapter</a>
                        <a class="btn btn-danger btn-block">Delete Draft</a>
                        <a class="btn btn-link btn-block" href="{{ route('creator.index') }}">Back</a>
                    </div>
            </td>

            <td>Title</td>
            <td>{{ $draft->title  }}</td>
        </tr>
        <tr>
            <td>Creator</td>
            <td>{{ $draft->creator }}</td>
        </tr>
        <tr>
            <td>Subject</td>
            <td>{{ $draft->subject }}</td>
        </tr>
        <tr>
            <td>Description</td>
            <td>{{ $draft->description }}</td>
        </tr>
        <tr>
            <td>Publisher</td>
            <td>{{ $draft->publisher }}</td>
        </tr>
        <tr>
            <td>Contributor</td>
            <td>{{ $draft->contributor }}</td>
        </tr>
        <tr>
            <td>Contributor</td>
            <td>{{ $draft->contributor }}</td>
        </tr>
        <tr>
            <td>Published At</td>
            <td>{{ $draft->published_at }}</td>
        </tr>
        <tr>
            <td>Type</td>
            <td>{{ $draft->type }}</td>
        </tr>

        <tr>
            <td>Format</td>
            <td>{{ $draft->format }}</td>
        </tr>

        <tr>
            <td>Source</td>
            <td>{{ $draft->source }}</td>
        </tr>

        <tr>
            <td>Language</td>
            <td>{{ $draft->language }}</td>
        </tr>

        <tr>
            <td>Relation</td>
            <td>{{ $draft->relation }}</td>
        </tr>

        <tr>
            <td>Coverage</td>
            <td>{{ $draft->coverage }}</td>
        </tr>

        <tr>
            <td>Rights</td>
            <td>{{ $draft->rights }}</td>
        </tr>
    </table>


    <h3>List of Chapter</h3>
    <ol class="draglist list-group">
        @foreach( $draft->chapters as $chapter )
            <li class="list-group-item">
                {{ $chapter->title }} 
                <a href="{{ route('creator.chapter.show', array($draft->id, $chapter->id)) }}" class="btn btn-sm btn-success float-right">
                    View
                </a>
            </li>
        @endforeach
    </ol>

</div>

@endsection

@section('css')
@parent
<style>
body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.draglist li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.draglist li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
</style>

@endsection

@section('js')
@parent
<script src="{!! asset('js/jquery-sortable.js') !!}"></script>

<script>
    $(function  () {
        $("ol.draglist").sortable();
    });
</script>

@endsection
