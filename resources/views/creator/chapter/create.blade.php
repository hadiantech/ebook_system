@extends('layouts.dashboard.master') 
@section('title', 'Epub Creator') 
@section('subtitle', 'Add Chapter') 
@section('content')

<div class="col-sm-12 bg-white border p-4">

    {{ Form::open(['url' => route('creator.chapter.store', $draft->id), 'files' => 'true']) }}
      
    <div class="form-group">
        {{ Form::label('title', 'Title') }} 
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('body', 'Body') }}
         {{ Form::textarea('body', null, array('class' => 'form-control my-editor')) }}
    </div>

    <a class="btn btn-link" href="{{ route('creator.show', $draft->id) }}">Back</a> 
    {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}
</div>
@endsection


@section('js')
@parent
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection