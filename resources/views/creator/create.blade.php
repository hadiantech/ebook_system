@extends('layouts.dashboard.master') @section('title', 'Epub Creator') @section('content')

<div class="col-sm-12 bg-white border p-4">

    {{ Form::open(['url' => route('creator.store'), 'files' => 'true']) }}
      
    <div class="form-group">
        {{ Form::label('title', 'Title') }} 
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('creator', 'Creator') }} 
        {{ Form::text('creator', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('subject', 'Subject') }} 
        {{ Form::text('subject', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }}
         {{ Form::textarea('description', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('publisher', 'Publisher') }} 
        {{ Form::text('publisher', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('contributor', 'Contributor') }} 
        {{ Form::text('contributor', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('published_at', 'Published At') }} 
        {{ Form::date('published_at', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('type', 'Type') }} 
        {{ Form::text('type', null, array('class' => 'form-control', 'required')) }}
    </div>
    
    <div class="form-group">
            {{ Form::label('format', 'Format') }} 
            {{ Form::text('format', null, array('class' => 'form-control', 'required')) }}
        </div>
    
    <div class="form-group">
        {{ Form::label('source', 'Source') }} 
        {{ Form::text('source', null, array('class' => 'form-control', 'required')) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('language', 'Language') }} 
        {{ Form::text('language', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('relation', 'Relation') }} 
        {{ Form::text('relation', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('coverage', 'Coverage') }} 
        {{ Form::text('coverage', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('rights', 'Rights') }} 
        {{ Form::text('rights', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('bookcover', 'Book Cover') }}
        <div class="input-group mb-3">
            <div class="custom-file">
                {{ Form::file('bookCover' ,['class' => "custom-file-input", 'id' => "bookCover", 'accept'=>"image/*", 'required']) }}
                <label class="custom-file-label" for="bookCover">Choose file</label>
            </div>
        </div>
              
        <img id="image_upload_preview" src="http://placehold.it/100x100" class="img-fluid img-thumbnail" alt="your image" />
    </div>


    <a class="btn btn-link" href="{{ route('creator.index') }}">Back</a> 
    {{ Form::submit('Create Draft', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}
</div>
@endsection


@section('js')
@parent
<script>
 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#bookCover").change(function () {
        readURL(this);
    });
</script>
@endsection