@extends('layouts.dashboard.master') 
@section('title', 'Packages Management') 
@section('content')

<div class="col-sm-12 mb-2 p-0">
    <div class="mb-3">
        <div class="d-flex flex-row-reverse">
            @can('add_packages')
                <a href="{{ route('packages.create') }}" class="btn btn-success">
                    <i class="fas fa-plus mr-2"></i> Add New
                </a>
            @endcan
        </div>
    </div>
</div>

<div class="col-sm-12 card p-4">
    <h3>Package List</h3>
    
    @foreach($packages as $package)
        @include('shared._package_view', ['package' => $package, 'view' => 'admin'])
    @endforeach

</div>


@endsection
