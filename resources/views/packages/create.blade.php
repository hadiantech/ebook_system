@extends('layouts.dashboard.master') 
@section('title', 'Package Management') 
@section('subtitle', 'Create')
@section('content')


<div class="col-lg-12 card p-4">
    
    {{ Form::open(array('url' => 'packages', 'files' => 'true')) }}
    <div class="form-group">
        {{ Form::label('title', 'Title') }} 
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }} 
        {{ Form::textarea('description', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('price', 'Price') }} 
        {{ Form::text('price', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('started_at', 'Started At') }} 
        {{ Form::date('started_at', null, array('class' => 'form-control', 'id' => "started_at", 'required')) }}
        <div class="form-check">
            {{Form::hidden('started_at_null',false)}}
            {{ Form::checkbox('started_at_null', true, false, array('class' => 'form-check-input', 'id' => "started_at_null")) }} 
            <label class="form-check-label" for="started_at_null">
                No Started Time
            </label>
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('ended_at', 'Ended At') }} 
        {{ Form::date('ended_at', null, array('class' => 'form-control', 'id' => "ended_at", 'required',)) }}
        <div class="form-check">
            {{Form::hidden('ended_at_null',false)}}
            {{ Form::checkbox('ended_at_null', true, false, array('class' => 'form-check-input', 'id' => "ended_at_null")) }} 
            <label class="form-check-label" for="ended_at_null">
                No Ended Time
            </label>
        </div>
    </div>

    <div class="form-group">
        {{ Form::label('image', 'Image') }} 
        <div class="input-group mb-3">
            <div class="custom-file">
                {{ Form::file('image' ,['class' => "custom-file-input", 'id' => "inputGroupFile01", 'accept'=>"image/*" , 'required']) }}
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
        </div>
    </div>

    <hr />
    <h5>Limit Setting</h5>
    <small>leave blank for unlimited</small>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon">Book Uploaded</div>
            </div>
            {{ Form::text('books_upload', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon">Book Created</div>
            </div>
            {{ Form::text('books_created', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon">Store</div>
            </div>
            {{ Form::text('store', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon">Store Inventory</div>
            </div>
            {{ Form::text('store_inv', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon">Promotion</div>
            </div>
            {{ Form::text('promotion', null, array('class' => 'form-control')) }}
        </div>
    </div>

    <br />
    <a class="btn btn-link" href="{{ route('packages.index') }}">Back</a>
    
    {{ Form::button('<i class="fa fa-check mr-1"></i> Add', array('class' => 'btn btn-success', 'type' => 'submit')) }} 
    {{ Form::close() }}
</div>

@endsection

@section('js')
@parent

<script>
    $('#inputGroupFile01').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#started_at_null').click (function () {
        var thisCheck = $(this);
        if (thisCheck.is (':checked')) {
            $('#started_at').attr('disabled', '');
        } else {
            $('#started_at').removeAttr('disabled');
        }
    });

    $('#ended_at_null').click (function () {
        var thisCheck = $(this);
        if (thisCheck.is (':checked')) {
            $('#ended_at').attr('disabled', '');
        } else {
            $('#ended_at').removeAttr('disabled');
        }
    });
</script>

@endsection